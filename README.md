# MultiMif

# PoneyMon
ALAMELLE Julien p1408714
CHADUIRON Jordan p1502734
FLANDIN Alban p1504017
LAFOIX Paul p1611978
NGUYEN Jenny p1502868
SACCHARIN Ambre 11302245  


## Comment compiler
Dans le répertoire du projet, exécuter `mvn compile` pour compiler les sources.
Ce projet nécessite les dépendances spécifiques suivantes :
* JDK 8 ou plus (testé avec la version 8.u144-1)
* JavaFX (testé avec la version 8.u121-1)
* JUnit (testé avec la version 4.8.1)

Pour créer une archive exécutable du projet, exécuter `mvn package`.
Cela produit deux fichiers :
* target/poneymon_fx-0.0.1-SNAPSHOT.jar
* target/poneymon_fx-0.0.1-SNAPSHOT-jar-with-dependencies.jar

##Comment exécuter
Le premier fichier précédemment construit ne contient pas de manifeste et doit 
donc être lancé avec 
`java -cp target/poneymon_fx-0.0.1-SNAPSHOT.jar -cp fr.univ_lyon1.info.m1.poneymon_fx.App`

Le second fichier en revanche, contient le manifeste et doit donc être lancé 
avec `java -jar target/poneymon_fx-0.0.1-SNAPSHOT-jar-with-dependencies.jar`

## Comment lancer les tests

Dans le répertoire, exécuter `mvn test`.

## Comment générer la documentation

Dans le répertoire du projet, exécuter `mvn javadoc:javadoc`.
Les fichiers seront placés dans target/site/apidocs/ (il est conseillé d'ouvrir
le fichier index.html présent dans ce dossier à l'aide du navigateur de son choix
afin de parcourir la documentation).

#Comment jouer en multijoueurs
Pour jouer à plusieurs, il faut s'assurer que tous les participants sont connectés sur le même 
réseau local. Il faut également ouvrir les ports 4500 en TCP et 8888 en UDP, notament sur Windows.


