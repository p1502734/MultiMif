/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univlyon1.info.m1.poneymonfx.model.obstacle;

import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Riviere;
import fr.univlyon1.info.m1.poneymonfx.model.GameObjectFactory;
import fr.univlyon1.info.m1.poneymonfx.model.ObstacleFactory;
import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.model.poney.TankPoney;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordan
 */
public class RiviereTest {
    
    private Riviere instance;
    
    @Before
    public void setUp() throws ObstacleFactory.ObstacleCreationException {
        instance = (Riviere) GameObjectFactory.createObstacle("Riviere", 0.5);
    }


    /**
     * Test of hit method, of class Riviere.
     */
    @Test
    public void testHit() {
        Poney p = new TankPoney();
        p.setVitesse(1.0);
        instance.hit(p);
        assertEquals(0.7, p.getVitesse(), 0.0001);
    }
    
}
