/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univlyon1.info.m1.poneymonfx.model;

import fr.univlyon1.info.m1.poneymonfx.model.Field;
import fr.univlyon1.info.m1.poneymonfx.model.ObstacleFactory;
import java.io.FileNotFoundException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordan
 */
public class FieldTest {
    
    private Field instance;
    
    @Before
    public void setUp() {
        instance = new Field(5);
    }


    /* @Test
    public void testGetInfoPoneys() {
        System.out.println("getInfoPoneys");
        Field instance = null;
        String expResult = "";
        String result = instance.getInfoPoneys();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /* @Test
    public void testStep() {
        System.out.println("step");
        Field instance = null;
        instance.step();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /**
     * Test of getNbPoneys method, of class Field.
     */
    @Test
    public void testGetNbPoneys() {
        assertEquals(5, instance.getNbPoneys());
    }

    /* @Test
    public void testGetPoneys() {
        System.out.println("getPoneys");
        Field instance = null;
        List<Poney> expResult = null;
        List<Poney> result = instance.getPoneys();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /* @Test
    public void testIsTermine() {
        System.out.println("isTermine");
        Field instance = null;
        boolean expResult = false;
        boolean result = instance.isTermine();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /* @Test
    public void testGetClassement() {
        System.out.println("getClassement");
        Field instance = null;
        String expResult = "";
        String result = instance.getClassement();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /* @Test
    public void testGetDistance() {
        Map<Integer, Double> result = instance.getDistance();
        assertEquals(0, result.get(0), 0.1);
    }*/
    
    /**
     * Test qu'un Json non valide fait renvoyé false à la fonction.
     * @throws ObstacleFactory.ObstacleCreationException
     * @throws FileNotFoundException 
     */
    @SuppressWarnings("JavaDoc")
    @Test
    public void testJsonInvalide() throws ObstacleFactory.ObstacleCreationException, FileNotFoundException {
        assertFalse(instance.loadLevel("./src/main/resources/testFaux.json"));
    }
}
