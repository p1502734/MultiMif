/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univlyon1.info.m1.poneymonfx.model.obstacle;

import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Barriere;
import fr.univlyon1.info.m1.poneymonfx.model.GameObjectFactory;
import fr.univlyon1.info.m1.poneymonfx.model.ObstacleFactory;
import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.model.poney.AquaPoney;
import fr.univlyon1.info.m1.poneymonfx.model.poney.TankPoney;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordan
 */
public class BarriereTest {
    
    private Barriere instance;
    
    @Before
    public void setUp() throws ObstacleFactory.ObstacleCreationException {
        instance = (Barriere)GameObjectFactory.createObstacle("Barriere", 0.5);
    }
    

    /**
     * Test of hit method, of class Barriere.
     */
    @Test
    public void testHitPoneyTank() {
        TankPoney p = new TankPoney();
        instance.hit(p);
        assertTrue(instance.isDestroyed());
    }

    @Test
    public void testHitPoney() {
        Poney p = new AquaPoney();
        instance.hit(p);
        assertFalse(instance.isDestroyed());
    }

    /**
     * Test of success method, of class Barriere.
     */
    @Test
    public void testSuccess() {
        Barriere b = new Barriere(0.5);
        assertEquals(0, instance.distanceTo(b), 0.1);
    }
    
}
