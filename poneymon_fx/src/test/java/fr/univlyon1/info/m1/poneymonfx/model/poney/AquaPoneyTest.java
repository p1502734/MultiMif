package fr.univlyon1.info.m1.poneymonfx.model.poney;

import fr.univlyon1.info.m1.poneymonfx.model.poney.AquaPoney;
import fr.univlyon1.info.m1.poneymonfx.model.GameObjectFactory;
import fr.univlyon1.info.m1.poneymonfx.model.ObstacleFactory;
import fr.univlyon1.info.m1.poneymonfx.model.PoneyFactory;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Riviere;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordan
 */
public class AquaPoneyTest {
   
    private AquaPoney instance;
    
    @Before
    public void setUp() throws PoneyFactory.PoneyCreationException {
        instance = (AquaPoney) GameObjectFactory.createPoney("Aqua");
    }
    

    /**
     * Test of riviere method, of class AquaPoney.
     */
    @Test
    public void testRiviere() throws ObstacleFactory.ObstacleCreationException {
        Riviere r = (Riviere)GameObjectFactory.createObstacle("Riviere", 0.5);
        double temp = instance.getVitesse();
        instance.riviere();
        assertEquals(1.5 * temp, instance.getVitesse(), 0.00000000000001);
    }
    
}
