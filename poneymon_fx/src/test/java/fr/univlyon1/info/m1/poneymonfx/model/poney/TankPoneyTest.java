package fr.univlyon1.info.m1.poneymonfx.model.poney;

import fr.univlyon1.info.m1.poneymonfx.model.poney.TankPoney;
import fr.univlyon1.info.m1.poneymonfx.model.GameObjectFactory;
import fr.univlyon1.info.m1.poneymonfx.model.ObstacleFactory;
import fr.univlyon1.info.m1.poneymonfx.model.PoneyFactory;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Barriere;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordan
 */
public class TankPoneyTest {
    
    private TankPoney instance;
    
    @Before
    public void setUp() throws PoneyFactory.PoneyCreationException {
        instance = (TankPoney) GameObjectFactory.createPoney("Tank");
    }
    


    /**
     * Test of barriere method, of class TankPoney.
     */
    @Test
    public void testBarriere() throws ObstacleFactory.ObstacleCreationException {
        double temp = instance.getVitesse();
        Barriere b = (Barriere)GameObjectFactory.createObstacle("Barriere", 0.5);
        assertTrue(instance.barriere(b));
        assertEquals(temp, instance.getVitesse(), 0.000000001);
    }
    
}
