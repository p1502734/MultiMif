package fr.univlyon1.info.m1.poneymonfx.model;

import fr.univlyon1.info.m1.poneymonfx.model.GameObject;
import fr.univlyon1.info.m1.poneymonfx.model.GameObjectFactory;
import fr.univlyon1.info.m1.poneymonfx.model.PoneyFactory;
import fr.univlyon1.info.m1.poneymonfx.model.poney.NianPoney;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author Jordan
 */
public class NianPoneyTest {

    private NianPoney model;

    @Before
    public void setUp() throws PoneyFactory.PoneyCreationException {
        model = (NianPoney)GameObjectFactory.createPoney("Nian");
    }
    
    @Test
    public void testId() {
        assertEquals(GameObject.nbObj , model.getId());
    }
    
    @Test
    public void testRainbowPath() {
        assertEquals("assets/pony-blue-rainbow.gif" , model.getRainbowAssetPath());
    }
    
    @Test
    public void testNormalPath() {
        assertEquals("assets/pony-blue-running.gif" , model.getNormalAssetPath());
    }
    
    @Test
    public void testDitancePoneys() {
        NianPoney p = new NianPoney();
        assertEquals(0.0, model.distanceTo(p), 0.1);
    }

    /*
    @Test
    public void testStep() {
        double temp = model.getProgression() + model.getVitesse();
        model.step();
        assertEquals(temp, model.getProgression(), 0.00000000001);
    }
*/


    /**
     * Test of toNian method, of class NianPoney.
     */
    @Test
    public void testToNian() {
        double temp = model.getVitesse();
        model.power();
        assertEquals(temp * 2, model.getVitesse(), 0.00000000000000001);
        assertTrue(model.isUsingPower());
        assertFalse(model.canUsePower());
    }

    /**
     * Test of distanceTo method, of class NianPoney.
     */
    @Test
    public void testDistanceTo() {
        NianPoney p = new NianPoney();
        double result = model.distanceTo(p);
        assertEquals(0.0, result, 0.01);
    }
}
