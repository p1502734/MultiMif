package fr.univlyon1.info.m1.poneymonfx.model.poney;

import fr.univlyon1.info.m1.poneymonfx.model.poney.GhostPoney;
import fr.univlyon1.info.m1.poneymonfx.model.GameObjectFactory;
import fr.univlyon1.info.m1.poneymonfx.model.ObstacleFactory;
import fr.univlyon1.info.m1.poneymonfx.model.PoneyFactory;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Barriere;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Riviere;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordan
 */
public class GhostPoneyTest {
    
    private GhostPoney instance;
    
    @Before
    public void setUp() throws PoneyFactory.PoneyCreationException {
        instance = (GhostPoney)GameObjectFactory.createPoney("Ghost");
    }

    /**
     * Test of riviere method, of class GhostPoney.
     */
    @Test
    public void testRiviere() throws ObstacleFactory.ObstacleCreationException {
        double temp = instance.getVitesse();
        Riviere r = (Riviere)GameObjectFactory.createObstacle("Riviere", 0.5);
        instance.riviere();
        assertEquals(temp, instance.getVitesse(), 0.000001);
    }

    /**
     * Test of barriere method, of class GhostPoney.
     */
    @Test
    public void testBarriere() throws ObstacleFactory.ObstacleCreationException {
        double temp = instance.getVitesse();
        Barriere b = (Barriere)GameObjectFactory.createObstacle("Barriere", 0.5);
        assertFalse(instance.barriere(b));
        assertEquals(temp, instance.getVitesse(), 0.000001);
        
    }
    
}
