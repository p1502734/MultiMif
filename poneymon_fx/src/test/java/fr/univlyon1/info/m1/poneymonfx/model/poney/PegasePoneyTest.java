package fr.univlyon1.info.m1.poneymonfx.model.poney;

import fr.univlyon1.info.m1.poneymonfx.model.poney.PegasePoney;
import fr.univlyon1.info.m1.poneymonfx.model.GameObjectFactory;
import fr.univlyon1.info.m1.poneymonfx.model.ObstacleFactory;
import fr.univlyon1.info.m1.poneymonfx.model.PoneyFactory;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Ravin;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordan
 */
public class PegasePoneyTest {

    private PegasePoney instance;
    
    @Before
    public void setUp() throws PoneyFactory.PoneyCreationException {
        instance = (PegasePoney) GameObjectFactory.createPoney("Pegase");
    }

    /**
     * Test of ravin method, of class PegasePoney.
     */
    @Test
    public void testRavin() throws ObstacleFactory.ObstacleCreationException {
        double temp = instance.getProgression();
        double temp2 = instance.getVitesse();
        Ravin r = (Ravin)GameObjectFactory.createObstacle("Ravin", 0.5);
        instance.ravin();
        assertEquals(temp, instance.getProgression(), 0.0000001);
        assertEquals(temp2, instance.getVitesse(), 0.0000001);
    }
    
}
