/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univlyon1.info.m1.poneymonfx.model.obstacle;

import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Ravin;
import fr.univlyon1.info.m1.poneymonfx.model.GameObjectFactory;
import fr.univlyon1.info.m1.poneymonfx.model.ObstacleFactory;
import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.model.poney.AquaPoney;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordan
 */
public class RavinTest {
    
    private Ravin instance;
    
    @Before
    public void setUp() throws ObstacleFactory.ObstacleCreationException {
        instance = (Ravin) GameObjectFactory.createObstacle("Ravin", 0.5);
    }


    /**
     * Test of hit method, of class Ravin.
     */
    @Test
    public void testHit() {
        Poney p = new AquaPoney();
        instance.hit(p);
        assertEquals(0.0, p.getVitesse(), 0.0001);
    }
    
}
