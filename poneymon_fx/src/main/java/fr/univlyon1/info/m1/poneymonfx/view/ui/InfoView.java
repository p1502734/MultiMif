package fr.univlyon1.info.m1.poneymonfx.view.ui;

import fr.univlyon1.info.m1.poneymonfx.model.Field;
import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.tools.Observable.Observer;
import fr.univlyon1.info.m1.poneymonfx.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class InfoView extends VBox implements View, Observer {

    private final int width;
    private final int height;
    private final GraphicsContext gc;
    private final Canvas content;
    private final BorderPane bp;
    private Field field = null;
    private final Stage stage;

    /**
     * Une liste de données à afficher dans le tableau.
     */
    private final ObservableList<Poney> data = FXCollections.observableArrayList();
    /**
     * Un tableau d'affichage.
     */
    private final TableView<Poney> table = new TableView<>();

    /**
     * Constructeur.
     *
     * @param stage Espace cible
     * @param w Largeur du canvas contenant les infos
     * @param h Hauteur du canvas contenant les infos
     */
    public InfoView(Stage stage, int w, int h) {
        bp = new BorderPane();
        bp.setMinHeight(h);
        bp.setMinWidth(w);
        width = w;
        height = h;
        content = new Canvas(w, h - 50);
        gc = content.getGraphicsContext2D();
        content.setFocusTraversable(true);
        this.stage = stage;
        this.stage.setTitle("Score");
        this.stage.setWidth(300);
        this.stage.setHeight(300);
    }

    /**
     * Insertion d'un field et créationd du tableau.
     */
    public void setField(Field field) {
        boolean update = this.field != null;
        this.field = field;
        this.field.registerObserver(this);
        if (!update) {
            final Label label = new Label("Poney Score");
            label.setFont(new Font("Arial", 20));
            
            table.setEditable(false);

            TableColumn<Poney, Integer> position = new TableColumn<>("Position");
            position.setCellValueFactory(
                    new PropertyValueFactory<>("classement"));

            TableColumn<Poney, String> pseudo = new TableColumn<>("Pseudo");
            pseudo.setCellValueFactory(
                    new PropertyValueFactory<>("nom"));

            TableColumn<Poney, String> poneyColor = new TableColumn<>("Poney");
            poneyColor.setCellValueFactory(
                    new PropertyValueFactory<>("type"));

            TableColumn<Poney, String> nbTours = new TableColumn<>("Tours");
            nbTours.setCellValueFactory(
                    new PropertyValueFactory<>("nbTours"));

            table.getColumns().addAll(position, pseudo, poneyColor, nbTours);

            this.setSpacing(5);
            this.setPadding(new Insets(10, 0, 0, 10));
            this.getChildren().addAll(label, table);
            Scene scene = new Scene(new Group());
            ((Group) scene.getRoot()).getChildren().addAll(this);

            stage.setScene(scene);
        } else {
            data.clear();
        }

        for (int i = 0; i < field.getNbPoneys(); i++) {
            data.add(field.getPoneys().get(i));
        }

        table.setItems(data);

        stage.show();
    }

    /**
     * Méthode qui rafrachait le canvas.
     */
    @Override
    public void display() {
        gc.setFill(Color.LIGHTGRAY);
        gc.fillRect(0, 0, width, height);
        gc.setFill(Color.SALMON);
        gc.setFont(new Font(30));
        bp.setCenter(content);
    }

    @Override
    public void onObservableChanged(Object o) {
        if (o instanceof HashMap) {
            List<Poney> toSort = new ArrayList<>(((HashMap) o).values());
            List<Poney> poneys = Field.sort(toSort);
            data.clear();
            for (int j = 1; j <= poneys.size(); j++) {
                for (Poney poney : poneys) {
                    if (poney.getClassement() == j) {
                        data.add(poney);
                    }
                }
            }
        }
        if (o instanceof Boolean) {
            Platform.runLater(stage::close);
        }
    }
}
