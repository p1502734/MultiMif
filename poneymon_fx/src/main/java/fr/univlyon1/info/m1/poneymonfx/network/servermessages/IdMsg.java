package fr.univlyon1.info.m1.poneymonfx.network.servermessages;

/**
 * Classe de type ServerMessage signifiant que le serveur envoie le nouvel id du client.
 */
public class IdMsg extends ServerMessage {
    public final int id;

    public IdMsg(int id) {
        this.id = id;
    }
}
