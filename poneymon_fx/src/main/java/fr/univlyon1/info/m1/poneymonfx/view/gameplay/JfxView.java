package fr.univlyon1.info.m1.poneymonfx.view.gameplay;

import fr.univlyon1.info.m1.poneymonfx.controller.Controller;
import fr.univlyon1.info.m1.poneymonfx.model.Field;
import fr.univlyon1.info.m1.poneymonfx.model.Game;
import fr.univlyon1.info.m1.poneymonfx.tools.Observable.Observer;
import fr.univlyon1.info.m1.poneymonfx.view.View;
import fr.univlyon1.info.m1.poneymonfx.view.gameplay.subjective.FieldViewSubjective;
import fr.univlyon1.info.m1.poneymonfx.view.ui.InfoView;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Vue JFX.
 *
 * @author Jordan
 */
public class JfxView implements View, Observer {

    private final int width;
    private final int height;

    private final Controller controller;

    private FieldViewSubjective fieldView;
    private final MenuView menuView;
    private final LobbyView lobbyView;
    private final InfoView infoView;
    private final Group root;

    /**
     * Constructeur.
     *
     * @param stage Fenetre
     * @param w largeur
     * @param h hauteur
     */
    public JfxView(Stage stage, int w, int h, Game game) {
        this.controller = game.getController();
        game.getClient().registerObserver(this);
        width = w;
        height = h;

        infoView = new InfoView(new Stage(), width, height);

        menuView = new MenuView(width, height, game);
        menuView.setJfxView(this);

        root = new Group();

        lobbyView = new LobbyView(this, game.getClient(), this.width, this.height);

        root.getChildren().addAll(menuView, lobbyView);

        lobbyView.setVisible(false);
        menuView.displayMenu();
        Scene scene = new Scene(root);

        // On ajoute la scene a la fenetre et on affiche
        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest((WindowEvent event) -> controller.disconnect());
    }

    @Override
    public void display() {
        
    }

    @Override
    public void onObservableChanged(Object o) {
        if (o instanceof Field) {
            //On a reçu un field de gameClient
            //On peut créer la fieldView
            Field field = (Field) o;
            fieldView = new FieldViewSubjective(width, height, field);
            fieldView.setC(controller);
            fieldView.setPseudoJoueurs();
            field.registerObserver(this);

            Platform.runLater(() -> {
                root.getChildren().add(fieldView);
                menuView.setVisible(false);
                lobbyView.setVisible(false);
                infoView.setField(field);
                controller.addView(infoView);
            });

        } else if (o instanceof Boolean) {
            Platform.runLater(lobbyView::endGame);
        }

    }

    /**
     * Montre la lobbyView.
     */
    void showLobbyView() {
        Platform.runLater(() -> {
            lobbyView.setVisible(true);
            if (fieldView != null) {
                fieldView.setVisible(false);
            }
            menuView.setVisible(false);
        });
    }

    /**
     * Montre le menu.
     */
    void showMenuChoixJoueur() {
        lobbyView.setVisible(false);
        menuView.setVisible(true);
        menuView.displayChoixTypeJoueur();
    }

    Controller getController() {
        return controller;
    }
}
