package fr.univlyon1.info.m1.poneymonfx.model;

/**
 * Interface qui factorise le fait de percuter un obstacle.
 * @author Jordan
 */
public interface Hitable {

    void hit(Poney p);
}
