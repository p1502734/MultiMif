package fr.univlyon1.info.m1.poneymonfx.model.obstacle;

import fr.univlyon1.info.m1.poneymonfx.model.Obstacle;
import fr.univlyon1.info.m1.poneymonfx.model.Poney;

/**
 * Barriere.
 * @author Jordan
 */
public class Barriere extends Obstacle {

    private boolean destroyed;
    private final double hauteur;
    private final String assetBrokenPath;
    
    /**
     * Constructeur de barrière.
     * @param progression progression à laquelle est située la barriere
     */
    public Barriere(double progression) {
        super("assets/Barriere.png");
        this.progression = progression;
        this.destroyed = false;
        this.assetBrokenPath = "assets/BarriereCasse.png";
        hauteur = 0.4;
    }

    @Override
    public void hit(Poney p) {
        if (!destroyed) {
            destroyed = p.barriere(this);
            notifyObservers(this);
        }
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public double getHauteur() {
        return hauteur;
    }

    public String getAssetBrokenPath() {
        return assetBrokenPath;
    }

    /**
     * Mets à jour l'état de la barrière si elle a été cassée par un autre poney.
     * @param barriere barrière qui a été cassée
     */
    public void update(Barriere barriere) {
        boolean anc = this.destroyed;
        this.destroyed = barriere.isDestroyed();
        if (anc != this.destroyed) {
            notifyObservers(this);
        }
    }
    
    
    
    

}
