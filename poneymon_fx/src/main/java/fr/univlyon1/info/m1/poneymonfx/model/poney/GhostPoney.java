package fr.univlyon1.info.m1.poneymonfx.model.poney;

import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Barriere;

/**
 * GhostPoney.
 * @author Jordan
 */
public class GhostPoney extends Poney {
    /**
     * Constructeur.
     */
    public GhostPoney() {
        super("assets/pony-ghost-running.gif", "assets/pony-ghost-running.gif");
    }

    /**
     * Effet de la riviere sur le GhostPoney.
     */
    @Override
    public void riviere() {
        vitesse *= 1;
    }

    /**
     * Effet de la barriere sur le GhostPoney.
     * @param b la barriere concernee
     * @return l'etat de la barriere apres le passage du poney
     */
    @Override
    public boolean barriere(Barriere b) {
        return false;
    }

    @Override
    public String getType() {
        return "Ghost";
    }

}
