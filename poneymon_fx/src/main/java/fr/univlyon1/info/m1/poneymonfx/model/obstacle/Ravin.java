package fr.univlyon1.info.m1.poneymonfx.model.obstacle;

import fr.univlyon1.info.m1.poneymonfx.model.Obstacle;
import fr.univlyon1.info.m1.poneymonfx.model.Poney;

/**
 * Ravin.
 * @author Jordan
 */
public class Ravin extends Obstacle {

    public Ravin(double position) {
        super("assets/Ravin.png");
        this.progression = position;
    }

    @Override
    public void hit(Poney p) {
        p.ravin();
    }
}
