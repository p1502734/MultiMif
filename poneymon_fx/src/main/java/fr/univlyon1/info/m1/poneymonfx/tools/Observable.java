package fr.univlyon1.info.m1.poneymonfx.tools;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implémentation thread safe du Pattern Observer.
 *
 * @author Jordan
 */
public class Observable {

    public interface Observer {

        void onObservableChanged(Object o);
    }

    private final Set<Observer> mObservers = Collections.newSetFromMap(
            new ConcurrentHashMap<Observer, Boolean>(0));

    /**
     * Abonne un Observer.
     *
     * @param obs Vue Observant le modèle
     */
    public void registerObserver(Observer obs) {
        if (obs != null) {
            mObservers.add(obs);
        } // this is safe due to thread-safe Set
    }

    /**
     * Désabonne un Observer.
     *
     * @param obs Vue Observant le modèle
     */
    public void unregisterObserver(Observer obs) {
        if (obs != null) {
            mObservers.remove(obs); // this is safe due to thread-safe Set
        }
    }

    /**
     * This method notifies currently registered observers about
     * Observable's change.
     */
    protected void notifyObservers(Object arg) {
        for (Observer observer : mObservers) { // this is safe due to thread-safe Set
            observer.onObservableChanged(arg);
        }
    }

}
