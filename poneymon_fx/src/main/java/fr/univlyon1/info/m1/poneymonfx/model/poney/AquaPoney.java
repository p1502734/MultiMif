package fr.univlyon1.info.m1.poneymonfx.model.poney;

import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import java.util.Timer;
import java.util.TimerTask;

/**
 * AquaPoney.
 *
 * @author Jordan
 */
public class AquaPoney extends Poney {

    /**
     * Constructeur.
     */
    public AquaPoney() {
        super("assets/pony-aqua-running.gif", "assets/pony-aqua-rainbow.gif");
    }

    /**
     * Effet de la riviere.
     */
    @Override
    public void riviere() {
        vitesse *= 1.3;
        vitesseMax *= 1.3;
        Timer t = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                vitesseMax = 1;
            }
        };
        t.schedule(task, 3000);
    }

    @Override
    public String getType() {
        return "Aqua";
    }

}
