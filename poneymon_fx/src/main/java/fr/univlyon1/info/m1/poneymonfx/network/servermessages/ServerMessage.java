package fr.univlyon1.info.m1.poneymonfx.network.servermessages;

import java.io.Serializable;

/**
 * Classe abstraite sérializable permettant de définir un message du serveur.
 */
public abstract class ServerMessage implements Serializable {
}
