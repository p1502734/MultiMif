package fr.univlyon1.info.m1.poneymonfx.view.gameplay.subjective;

import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.model.Power;
import fr.univlyon1.info.m1.poneymonfx.tools.Observable;
import fr.univlyon1.info.m1.poneymonfx.view.View;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

/**
 * Vue du poney.
 *
 * @author Jordan
 */
public class PoneyViewSubjective implements View, Observable.Observer {

    private Image poney;
    private Image poneyNormal;
    private Image poneyNian;
    private final GraphicsContext gc;
    private double x;
    private double y;
    private double sol;
    private Poney model;
    private String pseudoJoueur;

    /**
     * Constructeur.
     *
     * @param gc Contexte graphique
     */
    PoneyViewSubjective(GraphicsContext gc) {
        this.gc = gc;
    }

    /**
     * Affichage du poney.
     */
    @Override
    public void display() {
        gc.drawImage(poney, x - poney.getWidth() / 2.0, y);
        gc.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 16));
        gc.setFill(Color.RED);
        gc.fillText(pseudoJoueur, x - poney.getWidth() / 4.0, y);
    }

    void setSol(double sol) {
        this.sol = sol;
    }

    void setX(double x) {
        this.x = x;
    }

    void setY(double y) {
        this.y = y;
    }

    public Poney getModel() {
        return model;
    }

    /**
     * Assigne le modele de chaque poney.
     *
     * @param model le pm
     */
    public void setModel(Poney model) {
        this.model = model;
        this.model.registerObserver(this);
        this.poneyNormal = new Image(model.getNormalAssetPath());
        this.poneyNian = new Image(model.getRainbowAssetPath());
        this.poney = poneyNormal;
    }

    @Override
    public void onObservableChanged(Object o) {
        if (o instanceof Power && ((Power) o).isUsingPower()) {
            poney = poneyNian;
        } else {
            poney = poneyNormal;
        }
        y = sol - poney.getHeight()
                - ((Poney) o).getHauteur() * (gc.getCanvas().getHeight() / 2.0);

    }

    void setPseudoJoueur(String pseudoJoueur) {
        this.pseudoJoueur = pseudoJoueur;
    }

}
