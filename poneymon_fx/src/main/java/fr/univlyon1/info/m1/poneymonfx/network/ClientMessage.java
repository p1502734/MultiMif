package fr.univlyon1.info.m1.poneymonfx.network;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum permettant de définir les messages envoyés par le client au serveur.
 * Contient des méthodes permettant la conversion Integer to Enum (et inversement)
 */
public enum ClientMessage {
    DISCONNECT(5),
    START_GAME(0),
    SWITCH(1), //Switch to spectator or player
    JUMP(2),
    POWER(3),
    USER_MODEL_READY_TO_RECEIVE(4), //Message envoyé par le client pour notifier d'une lecture
    // non bloquante sur l'ObjectInputStream
    READY(6),
    REPLAY(7); //The game is ready


    private int value;
    private static final Map<Integer, ClientMessage> map = new HashMap<>();

    ClientMessage(int i) {
        value = i;
    }

    static {
        for (ClientMessage m :
                ClientMessage.values()) {
            map.put(m.value, m);
        }
    }

    public static ClientMessage valueOf(int value) {
        return map.get(value);
    }

    public int getValue() {
        return value;
    }
}