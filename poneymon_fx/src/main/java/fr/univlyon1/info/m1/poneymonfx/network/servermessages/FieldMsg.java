package fr.univlyon1.info.m1.poneymonfx.network.servermessages;

import fr.univlyon1.info.m1.poneymonfx.model.Field;

public class FieldMsg extends ServerMessage {
    public final Field field;

    public FieldMsg(Field field) {
        this.field = field;
    }
}
