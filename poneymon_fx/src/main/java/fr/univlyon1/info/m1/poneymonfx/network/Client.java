package fr.univlyon1.info.m1.poneymonfx.network;

import fr.univlyon1.info.m1.poneymonfx.model.UserModel;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;

/**
 * Classe permettant de stocker des informations sur un client (coté serveur).
 * Contient les objets permettant la communication (entrée et sortie),
 * ainsi qu'un identifiant permettant le lien avec le modèle.
 */
class Client {
    private ObjectOutputStream oos;
    private DataInputStream dis;
    private final Socket s;
    int id; //L'identifiant du client dans la liste du poney (-1 si spectateur)
    private boolean isConnected = true;
    boolean admin;
    UserModel userModel;

    /**
     * Créé l'ObjectOutputStream utilisé pour envoyer l'état du jeu au client,
     * ainsi qu'un DataInputStream utilisé pour recevoir les actions du client.
     *
     * @param s  La socket client recue par accept
     * @throws IOException Si erreur sur la socket.
     */
    Client(Socket s, boolean admin) throws IOException {
        this.s = s;
        this.admin = admin;
        oos = new ObjectOutputStream(s.getOutputStream());
        dis = new DataInputStream(s.getInputStream());
    }

    void quit() throws IOException {
        isConnected = false;
        s.close();
    }

    /**
     * Retourne le message reçu par le client, null s'il n'y a rien à lire.
     *
     * @return Le message du client, null sinon.
     * @throws IOException Si erreur sur la socket client.
     */
    ClientMessage getMessage() throws IOException {
        if (dis.available() > 0) {
            return ClientMessage.valueOf(dis.read());
        }
        return null;
    }

    /**
     * Lit de manière bloquante un objet envoyé par le client.
     * @return L'objet reçu.
     * @throws IOException Erreur sur la socket.
     */
    Object getObject() throws IOException {
        try {
            return new ObjectInputStream(s.getInputStream()).readUnshared();
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    /**
     * Envoie l'objet o (sérializable) au client.
     *
     * @param o L'objet à envoyer.
     * @throws IOException Si erreur sur la socket.
     */
    void sendObject(Serializable o) throws IOException {
        if (isConnected) {
            oos.reset();
            oos.writeObject(o);
        }
    }

    void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }
}
