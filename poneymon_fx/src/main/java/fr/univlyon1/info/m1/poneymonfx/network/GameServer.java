package fr.univlyon1.info.m1.poneymonfx.network;

import fr.univlyon1.info.m1.poneymonfx.model.Field;
import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.model.UserModel;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.FieldMsg;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.IdMsg;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.RaceEndMsg;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.ServerInfoMsg;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.ServerMessage;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.UpdateBarrieresMsg;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.UpdatePoneysMsg;

import java.io.IOException;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Deque;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

/**
 * Classe runnable représentant un serveur du jeu. Ecoute les requetes broadcast
 * de recherche des clients, gère un nombre de joueurs/spectateurs. Contient un
 * client administrateur qui peut lancer la partie. Fait tourner le modèle du
 * jeu.
 */
public class GameServer implements Runnable {

    private ServerSocket serverSocket;

    private final LinkedList<Client> clientsList = new LinkedList<>();

    //deque of available IDs for the new clients
    private final Deque<Integer> idStack = new LinkedList<>();

    private final ServerInfo info;

    private Field field;

    private final Timer timer;

    private boolean ended;
    

    /**
     * Constructeur. Crée le serveur.
     *
     * @param port Le port d'écoute.
     * @param nbPlayersMax Le nombres de joueurs maximum.
     * @param nbSpectatorsMax Le nombre de spectateurs maximum.
     */
    public GameServer(String name, int port, int nbPlayersMax, int nbSpectatorsMax) {
        //We find our ip
        String ip = getFirstPossibleIpV4();
        info = new ServerInfo(name
                + "," + ip
                + "," + port
                + "," + 0
                + "," + nbPlayersMax
                + "," + 0 + ","
                + nbSpectatorsMax);

        //rempli la liste d'ID dispo
        for (int i = 0; i < nbPlayersMax; i++) {
            idStack.add(i);
        }

        //We launch the server with method run
        Thread t = new Thread(this);
        t.setDaemon(true);
        t.start();

        timer = new Timer();
    }

    /**
     * Démarrage du serveur.
     */
    public void run() {

        try {
            serverSocket = new ServerSocket(info.port);
            serverSocket.setSoTimeout(1000); //Throw exception every second at accept

            //Open listening for broadcast discovery
            Thread discoveryThread = new Thread(DiscoveryThread.getInstance().setServer(this));
            discoveryThread.start();

            lobby();
            while (!clientsList.isEmpty()) {
                lobby();
            }

            quit();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException ex) {
            Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
            Thread.currentThread().interrupt();
        }

    }

    private void lobby() throws InterruptedException {
        boolean startGame = false;
        //Waiting clients
        try {
            while (!startGame) {
                try {
                    acceptOneClient();
                } catch (SocketTimeoutException e) {
                    //Toutes les secondes, on regarde si les clients ont des actions à réaliser
                    //Ou si le serveur est plein
                    startGame = checkInputClientsLobby();

                }
            }
            //Start the game
            startGame();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Teste si le serveur est plein.
     *
     * @return true si le nombre de clients est égal au nombre max de clients, false sinon.
     */
    private boolean serverFull() {
        return clientsList.size() >= info.nbPlayersMax + info.nbSpectatorsMax;
    }

    /**
     * Accepte un nouveau client si le serveur n'est pas plein. Fonction
     * bloquante 1 seconde(en cas de place disponible). Attribue au client un
     * identifiant le liant au modèle contrôlé, -1 s'il est spectateur.
     *
     * @throws IOException si le serveur est plein, on après une seconde
     */
    private void acceptOneClient() throws IOException {
        if (serverFull()) {
            throw new SocketTimeoutException();
        }
        //Sinon
        Socket sClient = serverSocket.accept();

        int nbClient = clientsList.size();
        boolean admin = nbClient == 0; //Premier client : c'est l'administrateur.

        //We create the client
        Client c = new Client(sClient, admin);

        //Normalement, le client nous envoie un message pour
        // nous avertir de son envoi de son UserModel
        // On attend son signal, si on ne le reçoit pas, on déconnecte le client.
        try {
            launchAndWaitWithTimeOut(new Thread(() -> {
                while (true) {
                    try {
                        if (c.getMessage() == ClientMessage.USER_MODEL_READY_TO_RECEIVE) {
                            return;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }), 3000);
        } catch (TimeoutException e) {
            //On déconnecte le client
            sClient.close();
            //On s'arrête
            return;
        }
        //On a reçu le message, on peut donc lire son UserModel
        Object receive = c.getObject();
        if (!(receive instanceof UserModel)) {
            sClient.close();
            return;
        }
        //On set l'user model
        UserModel userModel = (UserModel) receive;
        c.setUserModel(userModel);

        clientsList.add(c);

        //On choisit son rôle (spectateur/joueur)
        int idClient = -1;
        if (idStack.peek() == null) {
            info.addSpectator(c);
        } else {
            idClient = info.addPlayer(c, idStack.remove());
        }

        c.sendObject(new IdMsg(idClient));
        //On envoie l'info à tous les clients
        send(new ServerInfoMsg(info));

        showNbClients();
    }

    /**
     * Démarre le thread et attend un certain temps avant de throw une exception
     * de timeOut.
     *
     * @param t Le thread.
     * @param endTime La durée en ms avant timeout
     */
    private void launchAndWaitWithTimeOut(Thread t, long endTime) throws TimeoutException {
        t.start();

        long endTimeMillis = System.currentTimeMillis() + endTime;
        while (t.isAlive()) {
            if (System.currentTimeMillis() > endTimeMillis) {
                throw new TimeoutException();
            }
        }
    }

    /**
     * Affiche une ligne pour connaitre le nombre de clients. Fonction
     * temporaire (console)
     */
    private void showNbClients() {
        System.out.println("Players : " + info.nbPlayers + "/" + info.nbPlayersMax
                + "\tSpectators : "
                + info.nbSpectators + "/" + info.nbSpectatorsMax);
    }

    /**
     * Début du jeu.
     *
     * @throws IOException Sur checkInputClientsLobby
     */
    private void startGame() throws IOException, InterruptedException {
        //On crée l'instance du model
        field = new Field(info.nbPlayers);
        ended = false;


        Map<Integer, Poney> poneys = new HashMap<>();
        Map<Integer, String> joueurs = new HashMap<>();
        for (Client c : clientsList) {
            //pour chaque client, on va indiquer son poney
            if (c.id != -1) {
                //si c'est un spectateur, on ne crée pas son poney
                //on s'assure que son poney sera bien au début
                c.userModel.getCouleurPoney().reset();
                poneys.put(c.id, c.userModel.getCouleurPoney());
                joueurs.put(c.id, c.userModel.getPseudo());
            }
        }
        field.setPoneys(poneys);
        field.setJoueurs(joueurs);
        send(new FieldMsg(field));

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                try {
                    if (field.isTermine()) {
                        checkInputClientsLobby();
                        field.step();
                        send(new UpdatePoneysMsg(field.getPoneys()));
                        send(new UpdateBarrieresMsg(field.getBarrieresCassees()));
                    } else {
                        field.pause();
                        send(new RaceEndMsg());
                        ended = true;
                        this.cancel();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        timer.scheduleAtFixedRate(task, 0, 17);

        while (!ended && !clientsList.isEmpty()) {
            sleep(1);
        }
    }

    /**
     * Quitte le serveur.
     *
     * @throws IOException Si erreur sur la socket.
     */
    private void quit() throws IOException {
        serverSocket.close();
        DiscoveryThread.getInstance().stop();
    }

    /**
     * Envoie le message msg à tous les clients.
     *
     * @param msg Le ServerMessage à envoyer.
     * @throws IOException Si erreur sur les envois aux clients.
     */
    private void send(ServerMessage msg) throws IOException {

        for (Client client
                : clientsList) {
            client.sendObject(msg);
        }

    }

    /**
     * Regarde si les clients ont envoyé un message d'action au serveur, avant
     * le lancement du jeu.
     */
    private boolean checkInputClientsLobby() throws IOException {

        Iterator<Client> iterator = clientsList.iterator();
        while (iterator.hasNext()) {
            Client c = iterator.next();
            ClientMessage message = c.getMessage();
            if (message != null) {
                switch (message) {
                    case DISCONNECT: {
                        disconnectClient(c, iterator);
                        send(new ServerInfoMsg(info));
                        break;
                    }
                    case SWITCH: {
                        switchClient(c);
                        send(new ServerInfoMsg(info));
                        break;
                    }
                    case START_GAME: {
                        if (c.admin) {
                            return true;
                        }
                        break;
                    }
                    case JUMP: {
                        if (c.id != -1) {
                            field.jump(c.id);
                        }
                        break;
                    }
                    case POWER: {
                        if (c.id != -1) {
                            field.power(c.id);
                        }
                        break;
                    }
                    case REPLAY: {
                        send(new ServerInfoMsg(info));
                        break;
                    }
                    default:
                        break;
                }
            }

        }
        return false;
    }

    /**
     * Précondition : Le client c a envoyé un signal de déconnexion. Déconnecte
     * le client et le supprime de la liste à partir de son itérateur.
     *
     * @param c Le client à déconnecter.
     * @param it L'itérateur pour supprimer c de la liste.
     */
    private void disconnectClient(Client c, Iterator<Client> it) throws IOException {
        it.remove();
        if (c.id == -1) {
            info.removeSpectator(c);
        } else {
            //We remove a player
            info.removePlayer(c);
            if (c.id == 0) {
                setNewAdmin();
            } else {
                //we add his ID in the available ones
                idStack.push(c.id);
            }
        }
        c.quit();

        showNbClients();
    }

    /**
     * Essaie de changer le statut du client à jouer ou spectateur.
     * Pré-condition : le client est connecté.
     *
     * @param c Le client.
     */
    private void switchClient(Client c) throws IOException {
        int idClient = c.id;
        if (idClient != -1) {
            //the client is a player
            if (!info.nbSpectators.equals(info.nbSpectatorsMax)) {
                //He can become a spectator
                if (idClient == 0) {
                    //he was the admin, we need a new one
                    setNewAdmin();
                    c.admin = false;
                } else {
                    //he is not the admin
                    idStack.push(c.id);
                }
                info.addSpectator(c);
                info.removePlayer(c);
            }
        } else {
            //the client is a spectator
            if (idStack.peek() != null) {
                //He can become a player
                int newId = idStack.remove();
                if (newId == 0) {
                    c.admin = true;
                }
                info.addPlayer(c, newId);
                info.removeSpectator(c);
            }
        }
        //Envoi du nouvel id
        c.sendObject(new IdMsg(c.id));
        showNbClients();
    }

    /**
     * Chose a new admin in the game, put 0 on top of the ID list if there is no
     * player.
     *
     */
    private void setNewAdmin() throws IOException {
        if (info.nbPlayers != 0) {
            for (Client c : clientsList) {
                //searching for a player who can be the new admin
                if (c.id != -1) {
                    idStack.push(c.id);
                    c.id = 0;
                    c.admin = true;
                    c.sendObject(new IdMsg(c.id));
                    break;
                }
            }
        } else {
            //no player found, the next to join will be the new admin
            idStack.push(0);
        }
    }

    /**
     * Get the possible ipv4 address of the server in listIp.
     */
    private String getFirstPossibleIpV4() {
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp()) {
                    continue;
                }
                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    //Get only first ipv4
                    if (addr instanceof Inet4Address) {
                        return addr.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    /**
     * Retourne les informations du serveur formaté pour être lu par le
     * constructeur de ServerInfo.
     *
     * @return Le message à envoyer.
     */
    String getInfoString() {
        String res = info.name + "," + info.address + "," + info.port + ",";
        res += info.nbPlayers + "," + info.nbPlayersMax + ",";
        res += info.nbSpectators + "," + info.nbSpectatorsMax;
        return res;
    }

}
