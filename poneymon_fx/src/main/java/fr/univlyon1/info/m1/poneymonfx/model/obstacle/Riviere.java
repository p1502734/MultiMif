package fr.univlyon1.info.m1.poneymonfx.model.obstacle;

import fr.univlyon1.info.m1.poneymonfx.model.Obstacle;
import fr.univlyon1.info.m1.poneymonfx.model.Poney;

/**
 * Riviere.
 * @author Jordan
 */
public class Riviere extends Obstacle {

    public Riviere(double position) {
        super("assets/Riviere.png");
        this.progression = position;
    }

    @Override
    public void hit(Poney p) {
        p.riviere();
    }
}
