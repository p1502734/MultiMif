package fr.univlyon1.info.m1.poneymonfx.view.gameplay;

import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.model.UserModel;
import fr.univlyon1.info.m1.poneymonfx.network.GameClient;
import fr.univlyon1.info.m1.poneymonfx.network.ServerInfo;
import fr.univlyon1.info.m1.poneymonfx.tools.Observable.Observer;
import fr.univlyon1.info.m1.poneymonfx.view.View;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.OverrunStyle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.io.IOException;
import java.util.List;

import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class LobbyView extends BorderPane implements View, Observer {

    private final int width;

    private final JfxView jfxView;

    private final GameClient client;

    private ServerInfo serverInfo = null;

    private boolean init = false;

    private ListView<String> playersList = null;

    private ListView<String> spectatorList = null;

    private final Text sceneTitle = new Text();

    private Button btnLancerPartie;

    private Button btnDeconnexion;

    /**
     * Constructeur.
     *
     * @param view La JfxView.
     * @param client Le gameClient observé par lobbyView.
     */
    LobbyView(JfxView view, GameClient client, int w, int h) {
        width = w;
        this.client = client;
        client.registerObserver(this);
        this.jfxView = view;
        this.setPrefSize(width, h);
    }

    @Override
    public void display() {

    }

    /**
     * Créé les composants de notre vue en fonction du nombres de joueurs du
     * serveur. Pré-condition : init vaut false. Pöst-condition : init vaut
     * true.
     *
     * @param servInfo Les informations serveur.
     */
    private void init(ServerInfo servInfo) {

        serverInfo = servInfo;
        HBox titleCat = new HBox(width - 300);
        titleCat.setPadding(new Insets(50, 50, 50, 50));
        Text spect = new Text("Spectateur(s)");
        spect.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        Text play = new Text("Joueur(s)");
        play.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        titleCat.getChildren().addAll(play, spect);

        HBox maintitle = new HBox((width / 2.0) - 150);

        sceneTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        Button btnChangementPoney = new Button("Changement Poney");
        maintitle.getChildren().addAll(btnChangementPoney, sceneTitle);

        VBox title = new VBox();
        title.getChildren().addAll(maintitle, titleCat);

        this.setTop(title);

        playersList = new ListView<>();
        playersList.setPrefWidth(300);
        playersList.setEditable(false);
        ObservableList<String> items = FXCollections.observableArrayList();
        for (int i = 0; i < serverInfo.getNbPlayersMax(); ++i) {
            items.add("");
        }

        playersList.setCellFactory(param -> new ListCell<String>() {
            private ImageView imageView = new ImageView();

            @Override
            public void updateItem(String name, boolean empty) {
                super.updateItem(name, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else if (!name.equals("")) {
                    Image img
                            = new Image(getServerInfo().getPlayerList()
                                    .get(playersList.getItems().indexOf(name))
                                    .getCouleurPoney()
                                    .getNormalAssetPath());
                    imageView = new ImageView(img);
                    setText(name);
                    setTextOverrun(OverrunStyle.CENTER_ELLIPSIS);
                    setTextAlignment(TextAlignment.CENTER);
                    setGraphic(imageView);
                    setGraphicTextGap(-10);
                }
            }
        });

        playersList.setItems(items);

        spectatorList = new ListView<>();
        spectatorList.setEditable(false);
        items = FXCollections.observableArrayList();
        for (int i = 0; i < serverInfo.getNbSpectatorsMax(); ++i) {
            items.add("");
        }

        spectatorList.setCellFactory(param -> new ListCell<String>() {
            private ImageView imageView = new ImageView();

            @Override
            public void updateItem(String name, boolean empty) {
                super.updateItem(name, empty);

                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else if (!name.equals("")) {
                    Image img
                            = new Image(getServerInfo().getSpectatorList()
                                    .get(spectatorList.getItems().indexOf(name))
                                    .getCouleurPoney()
                                    .getNormalAssetPath());
                    imageView = new ImageView(img);
                    setText(name);
                    setGraphic(imageView);
                }
            }
        });

        spectatorList.setItems(items);

        VBox vBox = new VBox();
        vBox.setPrefWidth(100);
        vBox.setPadding(new Insets(10, 50, 50, 50));
        vBox.setSpacing(10);
        vBox.setAlignment(Pos.CENTER);

        btnLancerPartie = new Button("Lancer la partie");
        Button btnSwitch = new Button("Switch");
        btnDeconnexion = new Button("Déconnexion");

        btnLancerPartie.setMinWidth(vBox.getPrefWidth());
        btnSwitch.setMinWidth(vBox.getPrefWidth());
        btnDeconnexion.setMinWidth(vBox.getPrefWidth());

        vBox.getChildren().addAll(btnLancerPartie, btnSwitch, btnDeconnexion);

        if (client.getId() != 0) {
            //Non Admin
            btnLancerPartie.setDisable(true);
        }

        btnLancerPartie.setOnAction((ActionEvent i) -> jfxView.getController().start());

        btnSwitch.setOnAction((ActionEvent i) -> jfxView.getController().switchRole());

        btnDeconnexion.setOnAction((ActionEvent i) -> {
            try {
                client.quit();
                jfxView.showMenuChoixJoueur();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        this.setLeft(playersList);
        this.setRight(spectatorList);
        this.setCenter(vBox);
        this.setBottom(null);

        init = true;
    }

    private ServerInfo getServerInfo() {
        return serverInfo;
    }

    /**
     * Met à jour les listView et le nom du serveur.
     *
     * @param serverInfo l'info sur le serveur.
     */
    private void updateFields(ServerInfo serverInfo) {
        sceneTitle.setText(serverInfo.getName());
        updateListView(playersList, serverInfo.getPlayerList());
        updateListView(spectatorList, serverInfo.getSpectatorList());

    }

    @Override
    public void onObservableChanged(Object o) {
        if (o instanceof ServerInfo) {
            serverInfo = (ServerInfo) o;
            //On vérifie qu'on a créé nos éléments
            if (!init) {
                Platform.runLater(() -> {
                    init(serverInfo);
                    //updateFields(serverInfo);
                });
            }
            Platform.runLater(() -> updateFields(serverInfo));
        }

        if (init && o instanceof Integer) {
            //On a reçu un id
            Platform.runLater(() -> {
                if (client.getId() == 0) {
                    btnLancerPartie.setDisable(false);
                } else {
                    btnLancerPartie.setDisable(true);
                }
            });
        }

    }

    /**
     * Met à jour la listeView à partir de l'userList.
     *
     * @param listView La ListView à update.
     * @param userList La liste des userModel.
     */
    private void updateListView(ListView<String> listView, List<UserModel> userList) {
        listView.getItems().clear();
        int nbItems = listView.getItems().size();
        ObservableList<String> items = FXCollections.observableArrayList();

        for (UserModel user : userList) {
            items.add(user.getPseudo());
        }

        //On remplit de texte vide
        for (int i = items.size(); i < nbItems; ++i) {
            items.add("");
        }

        listView.setItems(items);
    }

    /**
     * The menu at the end of the game.
     */
    void endGame() {
        TableView table = new TableView();
        
        Label label = new Label("Poney Score");
        label.setFont(new Font("Arial", 20));

        table.setEditable(false);

        TableColumn position = new TableColumn("Position");
        position.setCellValueFactory(
                new PropertyValueFactory<Poney, Integer>("classement"));

        TableColumn pseudo = new TableColumn("Pseudo");
        pseudo.setCellValueFactory(
                new PropertyValueFactory<Poney, String>("nom"));

        TableColumn poneyColor = new TableColumn("Poney");
        poneyColor.setCellValueFactory(
                new PropertyValueFactory<Poney, String>("type"));

        table.getColumns().addAll(position, pseudo, poneyColor);

        ObservableList<Poney> data = FXCollections.observableArrayList();
        data.addAll(client.getField().getClassement());

        table.setItems(data);

        VBox vBox = new VBox();
        vBox.setSpacing(5);
        vBox.setPadding(new Insets(10, 0, 0, 10));
        vBox.getChildren().addAll(label, table);
        this.setCenter(vBox);

        VBox vBox2 = new VBox();
        vBox2.setPrefWidth(100);
        vBox2.setPadding(new Insets(10, 50, 50, 50));
        vBox2.setSpacing(10);
        vBox2.setAlignment(Pos.CENTER);

        Button btnRejouer = new Button("Rejouer");

        btnRejouer.setMinWidth(vBox2.getPrefWidth());
        btnDeconnexion.setMinWidth(vBox2.getPrefWidth());


        vBox2.getChildren().addAll(btnRejouer, btnDeconnexion);

        this.setTop(null);
        this.setRight(null);
        this.setLeft(null);
        this.setBottom(vBox2);

        btnRejouer.setOnAction((ActionEvent i) -> {
            init = false;
            jfxView.getController().rejouer();

        });

        jfxView.showLobbyView();
    }
}
