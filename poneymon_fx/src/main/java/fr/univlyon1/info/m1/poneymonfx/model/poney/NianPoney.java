package fr.univlyon1.info.m1.poneymonfx.model.poney;

import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.model.Power;
import java.util.Timer;
import java.util.TimerTask;

/**
 * NianPoney.
 *
 * @author Jordan
 */
public class NianPoney extends Poney implements Power {

    private boolean isNian;
    private boolean canNian;

    /**
     * Constructeur.
     */
    public NianPoney() {
        super("assets/pony-blue-running.gif", "assets/pony-blue-rainbow.gif");
        isNian = false;
        canNian = true;
    }

    /**
     * Passer en nian.
     */
    public void power() {
        if (this.canNian) {
            vitesseMax = 2;
            canNian = false;
            isNian = true;
            Timer t = new Timer();
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    vitesseMax = 1;
                    isNian = false;
                }
            };
            t.schedule(task, 5000);
        }
    }

    public boolean isUsingPower() {
        return isNian;
    }

    public boolean canUsePower() {
        return canNian;
    }

    @Override
    public String getType() {
        return "Nian";
    }

    @Override
    public void update(Power pw, Poney p) {
        super.update(p);
        this.isNian = pw.isUsingPower();
        this.canNian = pw.canUsePower();
        notifyObservers(this);
    }

    @Override
    public void reset() {
        super.reset();
        isNian = false;
        canNian = true;
    }
}
