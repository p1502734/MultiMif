package fr.univlyon1.info.m1.poneymonfx.model;

/**
 * Obstacle.
 * @author Jordan
 */
public abstract class Obstacle extends GameObject implements Hitable {

    private final String assetPath;
    
    protected Obstacle(String filename) {
        assetPath = filename;
    }

    public String getAssetPath() {
        return assetPath;
    }    
}
