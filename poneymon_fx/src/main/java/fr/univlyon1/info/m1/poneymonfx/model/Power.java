package fr.univlyon1.info.m1.poneymonfx.model;

/**
 * Interface commune à tous les poneys pouvant utiliser un pouvoir.
 * @author Jordan
 */
public interface Power {
    void power();
    
    boolean canUsePower();
    
    boolean isUsingPower();
    
    void update(Power pw, Poney p);
}
