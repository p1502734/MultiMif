package fr.univlyon1.info.m1.poneymonfx.network;

import fr.univlyon1.info.m1.poneymonfx.network.servermessages.ServerMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.SocketException;

/**
 * Classe runnable permettant l'écoute asynchrone du serveur.
 * Utilise l'objet ObjectInputStream pour recevoir les objets de type ServerMessage.
 * Notifie le GameClient d'une réception.
 */
class ReceiveServerObjectsThread implements Runnable {

    /**
     * Le client à notifier lors d'une réception de messages.
     */
    private final GameClient client;

    /**
     * L'objectInputStream utilisé pour recevoir des objets de type update.
     */
    private final ObjectInputStream ois;

    /**
     * Booléen pour stopper le thread.
     */
    private boolean stop = false;

    ReceiveServerObjectsThread(ObjectInputStream ois, GameClient client) {
        this.client = client;
        this.ois = ois;
    }

    @Override
    public void run() {
        while (!stop) {
            //We wait a new object to be read
            try {
                //We can cast because server only send ServerMessage
                ServerMessage serverMessage = (ServerMessage) ois.readUnshared();

                //We notify the gameClient
                client.processMessage(serverMessage);
            } catch (SocketException e) {
                stop = true;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Stoppe la boucle du thread en modifiant la valeur du booléen stop.
     */
    public void stop() {
        stop = true;
    }

}
