package fr.univlyon1.info.m1.poneymonfx.view.gameplay.subjective;

import fr.univlyon1.info.m1.poneymonfx.model.Obstacle;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Barriere;
import fr.univlyon1.info.m1.poneymonfx.tools.Observable;
import fr.univlyon1.info.m1.poneymonfx.view.View;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 * Vue d'affichage des obstacles.
 *
 * @author Jordan
 */
public class ObstacleViewSubjective implements View, Observable.Observer {

    private Image obstacle;
    private final GraphicsContext gc;
    private double x;
    private double y;
    private Obstacle model;

    /**
     * Constructeur.
     *
     * @param gc Contexte graphique
     */
    ObstacleViewSubjective(GraphicsContext gc) {
        this.gc = gc;
    }

    /**
     * Affecte le modèle à l'obstacle.
     *
     * @param model model cible
     */
    public void setModel(Obstacle model) {
        this.model = model;
        this.model.registerObserver(this);
        this.obstacle = new Image(model.getAssetPath());
    }

    void setX(double x) {
        this.x = x;
    }

    void setY(double y) {
        this.y = y;
    }

    public Obstacle getModel() {
        return model;
    }

    @Override
    public void display() {
        if (model instanceof Barriere) {
            gc.drawImage(obstacle, x - obstacle.getWidth() / 2, y - obstacle.getHeight());
        } else {
            gc.drawImage(obstacle, x - obstacle.getWidth() / 2, y - 9.0);
        }

    }

    @Override
    public void onObservableChanged(Object o) {
        if (o instanceof Barriere && ((Barriere) o).isDestroyed()) {
            obstacle = new Image(((Barriere) o).getAssetBrokenPath());
        }
    }

}
