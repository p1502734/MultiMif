package fr.univlyon1.info.m1.poneymonfx.model;

/**
 * Factory d'objets.
 * @author Jordan
 */
public class GameObjectFactory {
    
    private GameObjectFactory() {
        
    }

    public static Poney createPoney(String value) throws 
            PoneyFactory.PoneyCreationException {
        return PoneyFactory.create(value);
    }

    public static Obstacle createObstacle(String value, double 
            progression) throws ObstacleFactory.ObstacleCreationException {
        return ObstacleFactory.create(value, progression);
    }
}
