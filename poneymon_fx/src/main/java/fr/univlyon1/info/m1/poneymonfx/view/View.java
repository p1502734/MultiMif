package fr.univlyon1.info.m1.poneymonfx.view;

/**
 * Abstract vue.
 *
 * @author Jordan
 */
public interface View {
    void display();
}
