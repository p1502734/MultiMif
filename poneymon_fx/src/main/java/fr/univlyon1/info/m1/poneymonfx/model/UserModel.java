package fr.univlyon1.info.m1.poneymonfx.model;


import java.io.Serializable;

public class UserModel implements Serializable {

    private int id;
    private String pseudo;
    private Poney couleurPoney;

    UserModel(String pseudo, Poney couleurPoney) {
        this.pseudo = pseudo;
        this.couleurPoney = couleurPoney;
    }

    public int getId() {
        return id;
    }

    public Poney getCouleurPoney() {
        return couleurPoney;
    }

    void setCouleurPoney(Poney couleurPoney) {
        this.couleurPoney = couleurPoney;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void remove() {
        this.setPseudo(null);
        this.setCouleurPoney(null);
    }
}
