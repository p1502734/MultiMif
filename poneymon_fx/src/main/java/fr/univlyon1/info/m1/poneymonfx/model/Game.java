package fr.univlyon1.info.m1.poneymonfx.model;

import fr.univlyon1.info.m1.poneymonfx.controller.Controller;
import fr.univlyon1.info.m1.poneymonfx.network.GameClient;
import fr.univlyon1.info.m1.poneymonfx.network.GameServer;
import fr.univlyon1.info.m1.poneymonfx.network.ServerInfo;
import fr.univlyon1.info.m1.poneymonfx.tools.Observable;

import java.util.List;

/**
 * Classe représentant tout le jeu.
 */
public class Game extends Observable {

    private final UserModel user = new UserModel(null, null);
    private final GameClient client;
    private Controller c;

    public Game() {
        client = new GameClient(this);
    }

    /**
     * Crée un serveur et le rejoint.
     * @param name le nom.
     * @param port le port.
     * @param nbPlayers le nombre de joueurs max.
     * @param nbSpectators le nombre de spectateurs max.
     */
    public void createServerAndJoin(String name, int port, int nbPlayers, int nbSpectators) {
        new GameServer(name, port, nbPlayers, nbSpectators);
        joinServer("localhost", port);

    }

    /**
     * Rejoint un serveur.
     * @param address l'adresse.
     * @param port le port.
     */
    public void joinServer(String address, int port) {
        Thread t = new Thread(() -> 
            client.joinServer(address, port)
        );
        t.start();
    }

    /**
     * Rejoint un serveur.
     * @param serverInfo les infos du serveur.
     */
    public void joinServer(ServerInfo serverInfo) {
        Thread t = new Thread(() -> 
            client.joinServer(serverInfo)
        );
        t.start();
    }

    public void setController(Controller c) {
        this.c = c;
    }

    public Controller getController() {
        return c;
    }

    public List<ServerInfo> findServers() {
        return client.findServers();
    }

    /**
     * Cree le userModel.
     * @param pseudo le pseudo.
     * @param couleur le couleur.
     * @throws PoneyFactory.PoneyCreationException si probleme de creation.
     */
    public void setUserModel(String pseudo, int couleur)
            throws PoneyFactory.PoneyCreationException {
        user.setPseudo(pseudo);
        switch (couleur) {
            //TODO changer via enum?
            case 0:
                user.setCouleurPoney(PoneyFactory.create("Nian"));
                break;
            case 1:
                user.setCouleurPoney(PoneyFactory.create("Pegase"));
                break;
            case 2:
                user.setCouleurPoney(PoneyFactory.create("Ghost"));
                break;
            case 3:
                user.setCouleurPoney(PoneyFactory.create("Tank"));
                break;
            case 4:
                user.setCouleurPoney(PoneyFactory.create("Aqua"));
                break;
            default:
                user.setCouleurPoney(null);
        }
    }

    public UserModel getUser() {
        return user;
    }

    public GameClient getClient() {
        return client;
    }
}
