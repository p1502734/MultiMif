package fr.univlyon1.info.m1.poneymonfx.model.poney;

import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.model.Power;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Barriere;

/**
 * TankPoney.
 * @author Jordan
 */

public class TankPoney extends Poney implements Power {
    /**
     * Constructeur.
     */
    
    private boolean canTacle;
    private boolean isTacling;
    
    public TankPoney() {
        super("assets/pony-tank-running.gif", "assets/pony-tank-rainbow.gif");
        vitesseMax = 1.5;
    }

    /**
     * Effet de la barriere sur le TankPoney.
     * @param b la barriere concernee
     * @return l'état de la barriere après le passage du poney
     */
    @Override
    public boolean barriere(Barriere b) {
        return true;
    }

    @Override
    public double accelerationPoney() {
        acceleration = (vitesseMax - vitesse) / 2400;
        return acceleration;
    }

    public void power() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean canUsePower() {
        return canTacle;
    }

    @Override
    public boolean isUsingPower() {
        return isTacling;
    }

    @Override
    public String getType() {
        return "Tank";
    }

    @Override
    public void update(Power pw, Poney p) {
        super.update(p);
    }

    @Override
    public void reset() {
        super.reset();
        isTacling = false;
        canTacle = true;
    }
}
