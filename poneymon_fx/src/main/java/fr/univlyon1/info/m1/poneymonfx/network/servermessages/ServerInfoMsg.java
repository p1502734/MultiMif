package fr.univlyon1.info.m1.poneymonfx.network.servermessages;

import fr.univlyon1.info.m1.poneymonfx.network.ServerInfo;

public class ServerInfoMsg extends ServerMessage {
    public final ServerInfo info;

    public ServerInfoMsg(ServerInfo info) {
        this.info = info;
    }
}
