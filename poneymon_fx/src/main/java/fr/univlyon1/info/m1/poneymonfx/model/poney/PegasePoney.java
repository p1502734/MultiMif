package fr.univlyon1.info.m1.poneymonfx.model.poney;

import fr.univlyon1.info.m1.poneymonfx.model.Poney;

/**
 * PegasePoney.
 * @author Jordan
 */
public class PegasePoney extends Poney {
    /**
     * Constructeur.
     */
    public PegasePoney() {
        super("assets/pony-pegase-running.gif", "assets/pony-pegase-rainbow.gif");
    }

    /**
     * Effet du ravin sur le PegasePoney.
     */
    @Override
    public void ravin() {
        
    }

    @Override
    public String getType() {
        return "Pegase";
    }
}
