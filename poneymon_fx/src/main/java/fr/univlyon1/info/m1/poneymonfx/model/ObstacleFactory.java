package fr.univlyon1.info.m1.poneymonfx.model;

import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Barriere;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Ravin;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Riviere;

/**
 * ObstacleFactory.
 * @author Jordan
 */
public class ObstacleFactory {
    
    private ObstacleFactory() {
        
    }
   
    /**
     * Instancie un obstacle.
     * @param obstacleType type d'obstacle
     * @param progression positionnement de l'obstacle
     * @return instance d'un obstacle
     * @throws ObstacleFactory.ObstacleCreationException mauvais type d'obstacle
     */
    public static Obstacle create(String obstacleType, double progression) 
            throws ObstacleCreationException {
        switch (obstacleType) {
            case "Barriere":
                return new Barriere(progression);
            case "Ravin":
                return new Ravin(progression);
            case "Riviere":
                return new Riviere(progression);
            default:
                throw new ObstacleCreationException(
                        "Impossible de créer un obstacle " + obstacleType);
        }
    }

    public static class ObstacleCreationException extends Exception {

        ObstacleCreationException(String errorMessage) {
            super(errorMessage);
        }
    }
}
