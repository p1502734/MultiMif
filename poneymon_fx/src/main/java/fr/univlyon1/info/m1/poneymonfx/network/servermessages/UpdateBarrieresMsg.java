package fr.univlyon1.info.m1.poneymonfx.network.servermessages;

import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Barriere;
import java.util.Map;

public class UpdateBarrieresMsg extends ServerMessage {
    public final Map<Double, Barriere> oMap;

    public UpdateBarrieresMsg(Map<Double, Barriere> map) {
        this.oMap = map;
    }
}
