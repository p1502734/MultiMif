package fr.univlyon1.info.m1.poneymonfx.model;

import fr.univlyon1.info.m1.poneymonfx.tools.Observable;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Barriere;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Objects;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Field model.
 *
 * @author Jordan
 */
public class Field extends Observable implements Serializable {

    private Map<Integer, Poney> poneys;
    private final int nbPoneys;
    private boolean termine = false;
    private boolean pause = false;
    private int nbTours;
    private double distanceTour;
    private ConcurrentNavigableMap<Double, Obstacle> map = new ConcurrentSkipListMap<>();
    private int poneyFocused;
    private Map<Integer, String> joueurs = new HashMap<>();

    /**
     * Constructeur.
     */
    public Field(int nbPoneys) {
        this.nbPoneys = nbPoneys;
        this.poneys = new HashMap<>();

        URL l = this.getClass().getResource("/test.json");
        try {
            loadLevel(l.getFile());
        } catch (ObstacleFactory.ObstacleCreationException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Contructeur par copie.
     *
     * @param field field à copier
     */
    public Field(Field field) {
        poneyFocused = field.poneyFocused;
        joueurs = field.joueurs;
        poneys = field.poneys;
        nbPoneys = field.nbPoneys;
        termine = false;
        pause = false;
        nbTours = field.nbTours;
        distanceTour = field.distanceTour;
        this.map = field.map;
    }

    public void jump(int idPoney) {
        poneys.get(idPoney).jump();
    }

    /**
     * Active le pouvoir d'un poney donné.
     *
     * @param idPoney poney cible
     */
    public void power(int idPoney) {
        if (poneys.get(idPoney) instanceof Power) {
            ((Power) poneys.get(idPoney)).power();
        }
    }

    /**
     * Mets à jour les poneys en fonction du modèle recupéré depuis le serveur.
     *
     * @param map map de poneys recupérée depuis le serveur
     */
    public void update(Map<Integer, Poney> map) {
        for (Map.Entry<Integer, Poney> e : map.entrySet()) {
            if (poneys.get(e.getKey()) instanceof Power) {
                ((Power) poneys.get(e.getKey())).update((Power) e.getValue(), e.getValue());
            } else {
                poneys.get(e.getKey()).update(e.getValue());
            }
        }
        notifyObservers(poneys);
    }

    public int getPoneyFocused() {
        return poneyFocused;
    }

    public void setPoneyFocused(int nb) {
        poneyFocused = nb;
    }

    /**
     * Maj le model.
     */
    public void step() {
        if (!pause) {
            this.termine = true;
            poneys.values().stream().peek((Poney p) -> {
                if (p.getProgression() > distanceTour) {
                    p.incrementerTour();
                }
            }).peek((Poney p) -> {
                if (!Objects.equals(map.lowerKey(p.getProgression()),
                        map.lowerKey(p.getNextProg()))) {
                    map.lowerEntry(p.getNextProg()).getValue()
                            .hit(p);
                }
            }).peek(Poney::step).forEachOrdered(p -> {
                if (p.nbTours == nbTours) {
                    p.setVitesse(0);
                } else {
                    this.termine = false;
                }
            });
            updatePos();
            this.notifyObservers(this);
        }

    }

    /**
     * Charge les obstacles dans le map.
     *
     * @param filename chemin d'accès vers le JSON à parser
     * @return retourne un boolean qui indique si le json passé est bien valide
     * @throws ObstacleFactory.ObstacleCreationException erreur création obs
     * @throws FileNotFoundException fichier indiqué n'existe pas
     */
    public boolean loadLevel(String filename) throws
            ObstacleFactory.ObstacleCreationException, FileNotFoundException {
        JsonReader jsonReader = new JsonReader(new FileReader(filename));
        JsonObject jsonObject = new JsonParser().parse(jsonReader)
                .getAsJsonObject();
        JsonObject fieldInfos = jsonObject.getAsJsonObject("fieldInfo");
        int nbObs = fieldInfos.get("fieldObstacles").getAsInt();

        JsonArray arrObs = jsonObject.getAsJsonArray("obstacles");

        double distance = fieldInfos.get("fieldLength").getAsDouble();
        int nbToursJson = fieldInfos.get("fieldTurns").getAsInt();
        if (nbObs != arrObs.size() || (nbToursJson * distance > 200)) {
            return false;
        }

        distanceTour = distance;
        this.nbTours = nbToursJson;

        for (JsonElement obs : arrObs) {
            map.put(obs.getAsJsonObject()
                    .get("position").getAsDouble(),
                    GameObjectFactory.createObstacle(obs.getAsJsonObject()
                            .get("type").getAsString(), obs.getAsJsonObject()
                            .get("position").getAsDouble()));
        }

        return true;
    }

    public int getNbPoneys() {
        return nbPoneys;
    }

    /**
     * Renvoie les poneys sous forme d'une liste.
     *
     * @return liste de poneys
     */
    public Map<Integer, Poney> getPoneys() {
        return poneys;
    }

    public boolean isTermine() {
        return !termine;
    }

    /**
     * Renvoie le classement des poneys.
     *
     * @return Chaine de caractères contenant le classement
     */
    public List<Poney> getClassement() {
        List<Poney> toRet = new ArrayList<>();
        for (int i = 1; i <= this.poneys.size(); i++) {
            for (Poney poney : this.poneys.values()) {
                if (poney.classement == i) {
                    toRet.add(poney);
                }
            }
        }
        return toRet;
    }

    /**
     * Trie les poneys en fonctions de leur classement.
     *
     * @param poneys liste de poneys non triée
     * @return liste triée de poneys par leur classement
     */
    public static List<Poney> sort(List<Poney> poneys) {
        List<Poney> toRet = new ArrayList<>();
        for (int i = 1; i <= poneys.size(); i++) {
            for (Poney poney : poneys) {
                if (poney.classement == i) {
                    toRet.add(poney);
                }
            }
        }
        return toRet;
    }

    public ConcurrentNavigableMap<Double, Obstacle> getMap() {
        return map;
    }

    /**
     * Alterne la valeur du booléen pause.
     */
    public void pause() {
        pause = !pause;
    }

    public boolean isPaused() {
        return pause;
    }

    /**
     * Modifie les poneys avec ceux des joueurs.
     *
     * @param poneysMap une map de poney avec les poneys choisis
     */
    public void setPoneys(Map<Integer, Poney> poneysMap) {
        this.poneys = poneysMap;
    }

    public void setJoueurs(Map<Integer, String> joueurs) {
        this.joueurs = joueurs;
    }

    /**
     * Lie un poney au pseudo du joueur qui le contrôle.
     *
     * @return map des tous les poneys liés aux pseudos de leur joueur
     */
    public Map<Poney, String> getPoneysPseudo() {
        Map<Poney, String> toRet = new HashMap<>();
        poneys.forEach((key, value) -> {
            for (Map.Entry<Integer, String> e2 : joueurs.entrySet()) {
                if (Objects.equals(key, e2.getKey())) {
                    toRet.put(value, e2.getValue());
                    value.setNom(e2.getValue());
                }
            }
        });
        return toRet;
    }

    /**
     * Mets à jour les barrières en fonction du modèle recupéré depuis le
     * serveur.
     *
     * @param oMap map d'obstacles recupérée depuis le serveur
     */
    public void updateObstacles(Map<Double, Barriere> oMap) {
        oMap.forEach((key, value) -> ((Barriere) map.get(key)).update(value));
    }

    /**
     * Selectionne les barrières cassées du terrain et les renvoies.
     *
     * @return une map de barrières cassées
     */
    public Map<Double, Barriere> getBarrieresCassees() {
        Map<Double, Barriere> toRet = new HashMap<>();
        for (Double d : map.keySet()) {
            if (map.get(d) instanceof Barriere && ((Barriere) map.get(d)).isDestroyed()) {
                toRet.put(d, (Barriere) map.get(d));
            }
        }
        return toRet;
    }

    public void finCourse() {
        termine = true;
        notifyObservers(termine);
    }

    /**
     * Met à jour le classement de chaque poneys.
     */
    private void updatePos() {
        for (Poney p : poneys.values()) {
            if (p.getNbTours() != nbTours) {
                int i = 1;
                for (Poney p2 : poneys.values()) {
                    if (p != p2 && (p.distanceTo(p2) > 0.0 || p.getNbTours() < p2.getNbTours())) {
                        i++;
                    }
                }
                p.updatePos(i);
            }
        }
    }
}
