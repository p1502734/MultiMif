package fr.univlyon1.info.m1.poneymonfx.network.servermessages;

import fr.univlyon1.info.m1.poneymonfx.model.Poney;

import java.util.Map;

public class UpdatePoneysMsg extends ServerMessage {
    public final Map<Integer, Poney> pMap;

    public UpdatePoneysMsg(Map<Integer, Poney> pMap) {
        this.pMap = pMap;
    }
}
