package fr.univlyon1.info.m1.poneymonfx.network;

import fr.univlyon1.info.m1.poneymonfx.model.UserModel;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Stocke les informations sur un serveur.
 */
public class ServerInfo implements Serializable {
    String name;
    String address;
    Integer port;

    Integer nbPlayers;
    Integer nbPlayersMax;
    Integer nbSpectators;
    Integer nbSpectatorsMax;

    private final List<UserModel> playersList = new ArrayList<>();

    private final List<UserModel> spectatorList = new ArrayList<>();

    /**
     * Créé un objet serveur à partir d'un message de réponse d'un serveur.
     * Parse le message et associe les attributs.
     * @param message Le message envoyé par le dicoveryThread
     *                De la forme :
     *                name,address,port,nbPlayers,nbPlayersMax,nbSpectators,nbSpectatorsMax
     */
    ServerInfo(String message) {
        Field[] fieldClass = this.getClass().getDeclaredFields();
        String[] messageSplit = message.split(",");
        assert (fieldClass.length == messageSplit.length);

        for (int i = 0; i < messageSplit.length; i++) {
            try {
                if (fieldClass[i].getType() == Integer.class) {
                    fieldClass[i].set(this, Integer.parseInt(messageSplit[i]));
                } else {
                    fieldClass[i].set(this, messageSplit[i]);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Ajoute le client en tant que joueur.
     * @param c Le client (on associe son id)
     * @param id L'id du client
     * @return L'id du client
     */
    int addPlayer(Client c, int id) {
        nbPlayers++;
        playersList.add(c.userModel);
        c.id = id;
        return id;
    }

    /**
     * Enlève le client c aux joueurs.
     * @param c Le client.
     */
    void removePlayer(Client c) {
        nbPlayers--;
        playersList.remove(c.userModel);
    }

    /**
     * Ajoute le client en tant que spectateur (set son id à -1).
     * @param c Le client.
     */
    void addSpectator(Client c) {
        nbSpectators++;
        spectatorList.add(c.userModel);
        c.id = -1;
    }

    /**
     * Enl_ve le client c aux spectateurs.
     * @param c Le client.
     */
    void removeSpectator(Client c) {
        nbSpectators--;
        spectatorList.remove(c.userModel);
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Integer getPort() {
        return port;
    }

    public Integer getNbPlayers() {
        return nbPlayers;
    }

    public Integer getNbPlayersMax() {
        return nbPlayersMax;
    }

    public Integer getNbSpectators() {
        return nbSpectators;
    }

    public Integer getNbSpectatorsMax() {
        return nbSpectatorsMax;
    }

    public List<UserModel> getSpectatorList() {
        return spectatorList;
    }

    public List<UserModel> getPlayerList() {
        return playersList;
    }
}