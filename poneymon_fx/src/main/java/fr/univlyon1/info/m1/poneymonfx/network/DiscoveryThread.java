package fr.univlyon1.info.m1.poneymonfx.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe runnable utilisée par GameServer pour écouter les requêtes de recherche de clients.
 * Renvoie un message contenant les informations du serveur en UDP sur le port 8888.
 * Singleton: accès via DiscoveryThreadHolder.getInstance()
 */
public class DiscoveryThread implements Runnable {

    private DatagramSocket socket;
    private boolean stop = false;
    private GameServer server;

    @Override
    public void run() {

        try {
            //Keep a socket open to listen to all the UDP trafic that is destined for this port

            socket = DiscoveryThreadHolder.SOCKET;
            socket.setBroadcast(true);
            socket.setSoTimeout(1000); //Timeout every second

            while (!stop) {

                //Receive a packet
                byte[] recvBuf = new byte[15000];
                DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);

                try {
                    socket.receive(packet);
                } catch (SocketTimeoutException e) {
                    continue; //Non blocking : every second we recheck value of stop
                }
                //Packet received

                //See if the packet holds the right command (message)
                String message = new String(packet.getData()).trim();
                if (message.equals("DISCOVER_SERVER_REQUEST")) {

                    // On envoie la réponse au client, contenant aussi nos informations sur
                    // le serveur
                    String response = "DISCOVER_SERVER_RESPONSE;";
                    
                    response += server.getInfoString();


                    byte[] sendData = response.getBytes();

                    //Send a response
                    DatagramPacket sendPacket = new DatagramPacket(sendData,
                                                                    sendData.length,
                                                                    packet.getAddress(),
                                                                    packet.getPort());

                    socket.send(sendPacket);

                }
            }

            quit();

        } catch (IOException ex) {
            Logger.getLogger(DiscoveryThread.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void quit() {
        socket.close();
    }

    public void stop() {
        stop = true;
    }

    public DiscoveryThread setServer(GameServer server) {
        this.server = server;
        return this;
    }


    public static DiscoveryThread getInstance() {
        return DiscoveryThreadHolder.INSTANCE;
    }

    private static class DiscoveryThreadHolder {
        
        private DiscoveryThreadHolder() {
            
        }

        private static final DiscoveryThread INSTANCE = new DiscoveryThread();
        private static final DatagramSocket SOCKET = getSocket();

        private static DatagramSocket getSocket() {
            DatagramSocket res = null;
            try {
                res = new DatagramSocket(8888, InetAddress.getByName("0.0.0.0"));
            } catch (UnknownHostException | SocketException e) {
                e.printStackTrace();
            }
            return res;
        }
    }

}
