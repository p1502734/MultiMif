package fr.univlyon1.info.m1.poneymonfx.view.gameplay.subjective;

import fr.univlyon1.info.m1.poneymonfx.controller.Controller;
import fr.univlyon1.info.m1.poneymonfx.model.Field;
import fr.univlyon1.info.m1.poneymonfx.model.Obstacle;
import fr.univlyon1.info.m1.poneymonfx.model.Poney;
import fr.univlyon1.info.m1.poneymonfx.model.Power;
import fr.univlyon1.info.m1.poneymonfx.tools.Observable;
import fr.univlyon1.info.m1.poneymonfx.view.View;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.scene.text.Font;

/**
 * Vue du champ.
 *
 * @author Jordan
 */
public final class FieldViewSubjective extends BorderPane implements View, Observable.Observer {

    private final Canvas gameCanvas;
    private final GraphicsContext gc;
    private final int width;
    private final int height;
    private Controller c;
    private final List<PoneyViewSubjective> poneyList;
    private final List<ObstacleViewSubjective> obstacleList;
    private Poney selectedPoney;
    private Field model = null;
    private Image fondCiel;
    private Image fondTerre;

    /**
     * Constructeur.
     */
    public FieldViewSubjective(int w, int h, Field model) {
        this.setMinWidth(w);
        gameCanvas = new Canvas(w, h);
        width = w;
        height = h;
        fondCiel = new Image("assets/FondJeuCiel.png", width, height, false, true);

        fondTerre = new Image("assets/FondJeuTerre.png", width, height, false, true);
        
        /*
         * Permet de capturer le focus et donc les evenements clavier et
         * souris
         */
        gameCanvas.setFocusTraversable(true);
        gc = gameCanvas.getGraphicsContext2D();
        poneyList = new ArrayList<>();
        obstacleList = new ArrayList<>();
        setModel(model);
        if (model.getPoneyFocused() == -1) {
            selectedPoney = model.getPoneys().get(0);
        } else {
            selectedPoney = model.getPoneys().get(model.getPoneyFocused());
        }
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);
        hbox.setStyle("-fx-background-color: #336699;");



        if (model.getPoneyFocused() == -1) {
            hbox.getChildren().add(new Text("Infos :"));
            for (Poney pm : model.getPoneys().values()) {
                Button bInfos = new Button(pm.getClass().getSimpleName());
                bInfos.setOnAction(e -> selectedPoney = pm);
                hbox.getChildren().add(bInfos);
            }
        } else {
            Button bJump = new Button("Saut");
            bJump.setOnAction((ActionEvent e) ->
                    c.jump()
            );
            hbox.getChildren().add(bJump);
        }



        if (selectedPoney instanceof Power) {
            Button bPower = new Button("Pouvoir");
            bPower.setOnAction((ActionEvent e) -> 
                c.power()
            );
            hbox.getChildren().add(bPower);
        }

        this.setTop(hbox);
        this.setMinHeight(h + hbox.getHeight());
    }

    /**
     * Affecte le modèle à la vue.
     *
     * @param field modèle cible
     */
    private void setModel(Field field) {
        this.model = field;
        this.model.registerObserver(this);

        for (Poney pm : model.getPoneys().values()) {
            PoneyViewSubjective vue = new PoneyViewSubjective(gc);
            vue.setY(height / 2.0);
            vue.setSol(height / 2.0);
            vue.setModel(pm);
            this.poneyList.add(vue);
        }

        for (Map.Entry<Double, Obstacle> obs : model.getMap().entrySet()) {
            ObstacleViewSubjective vue = new ObstacleViewSubjective(gc);
            vue.setY(height / 2.0);
            vue.setModel(obs.getValue());

            this.obstacleList.add(vue);
        }
    }

    @Override
    public void display() {
        if (model.isPaused()) {
            displayPauseMessage();
            this.setCenter(gameCanvas);
        } else {
            gc.drawImage(fondCiel, 0, 0, width, height / 2.0);
            gc.drawImage(fondTerre, 0, height / 2.0, width, height);

            if (model.isTermine()) {
                PoneyViewSubjective pSelected = poneyList.get(0);
                for (PoneyViewSubjective p : poneyList) {
                    if (p.getModel().equals(selectedPoney)) {
                        pSelected = p;
                    } else {
                        p.display();
                    }
                }
                pSelected.display();

                this.obstacleList.forEach(ObstacleViewSubjective::display);
                
                this.setCenter(gameCanvas);
            }
        }
    }

    /**
     * Assigne à chaque poneyview le nom du joueur qui contôle le poney.
     */
    public void setPseudoJoueurs() {
        Map<Poney, String> pseudos = model.getPoneysPseudo();
        poneyList.forEach(pv -> 
            pseudos.keySet().stream().filter(p -> (pv.getModel().equals(p)))
                    .forEachOrdered(p -> 
                        pv.setPseudoJoueur(pseudos.get(p))
                    )
        );

    }

    private void displayPauseMessage() {
        gc.setFill(Color.BLACK);
        gc.setFont(new Font(44));
        gc.strokeText("Game paused...", (double) width / 4, (double) height / 4);
        gc.setFont(Font.getDefault());
    }

    /**
     * Affecte le controlleur à la vue.
     *
     * @param c controlleur cible
     */
    public void setC(Controller c) {
        this.c = c;
        c.addView(this);
        c.startTimer();
    }


    @Override
    public void onObservableChanged(Object o) {
        this.poneyList.forEach(p -> {
            double x = 0.5;
            if (p.getModel() != selectedPoney) {
                x += selectedPoney.distanceTo(p.getModel());
            }
            p.setX(x * this.width);
        });
        this.obstacleList.forEach((obs -> {
            double x = 0.5;
            x -= obs.getModel().distanceTo(selectedPoney);
            obs.setX(x * this.width);
        }));
    }
}
