package fr.univlyon1.info.m1.poneymonfx;

import fr.univlyon1.info.m1.poneymonfx.controller.Controller;
import fr.univlyon1.info.m1.poneymonfx.model.Game;
import fr.univlyon1.info.m1.poneymonfx.view.gameplay.JfxView;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Classe principale de l'application.
 *
 * s'appuie sur javafx pour le rendu
 */
public class App extends Application {

    /**
     * En javafx start() lance l'application.
     *
     * On cree le SceneGraph de l'application ici
     *
     * @see <a
     * href="http://docs.oracle.com/javafx/2/scenegraph/jfxpub-scenegraph.htm">jfxpub-scenegraph.htm</a>
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("Poneymon");

        //Create the game
        Game game = new Game();
        Controller controller = new Controller();
        game.setController(controller);
        controller.setGame(game);

        new JfxView(stage, 1000, 600, game);

        stage.setOnCloseRequest((WindowEvent t) -> {
            Platform.exit();
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
