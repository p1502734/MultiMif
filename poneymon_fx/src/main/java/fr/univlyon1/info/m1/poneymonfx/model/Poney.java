package fr.univlyon1.info.m1.poneymonfx.model;

import fr.univlyon1.info.m1.poneymonfx.model.obstacle.Barriere;

public abstract class Poney extends GameObject {

    protected double vitesse;
    protected double acceleration;
    int nbTours;
    int classement;
    private final String normalAssetPath;
    private final String rainbowAssetPath;
    private boolean isJumping = false;
    private double hauteur = 0d;
    protected double vitesseMax;
    private String type;
    private String nom;

    /**
     * Constructeur.
     * @param nAssetPath chemin vers l'asset normal
     * @param rAssetPath chemin vers l'asset arc-en-ciel
     */
    protected Poney(String nAssetPath, String rAssetPath) {
        progression = 0;
        acceleration = 0;
        vitesse = 0;
        nbTours = 0;
        vitesseMax = 1.0;
        normalAssetPath = nAssetPath;
        rainbowAssetPath = rAssetPath;
        type = this.getClass().getSimpleName();
    }

    /**
     * Mets à jour les infos d'un poney en fonction de son état sur le serveur.
     * @param p p recupéré depuis le serveur
     */
    public void update(Poney p) {
        this.vitesse = p.vitesse;
        this.vitesseMax = p.vitesseMax;
        this.acceleration = p.acceleration;
        nbTours = p.nbTours;
        this.isJumping = p.isJumping;
        this.hauteur = p.hauteur;
        classement = p.classement;
        progression = p.progression;
        notifyObservers(this);
    }

    /**
     * Choisir l'accélération du Poney.
     * @return la valeur de l'accélération
     */
    protected double accelerationPoney() {
        acceleration = (vitesseMax - vitesse) / 1200;
        return acceleration;
    }

    /**
     * Fait avancer le poney.
     */
    void step() {
        if (isJumping) {
            if (hauteur < 0.5 && vitesse > 0.1) {
                hauteur += vitesse / 20.0;
            } else {
                isJumping = false;
            }
        } else if (hauteur > 0.0) {
            hauteur -= vitesse / 20.0;
            if (hauteur < 0) {
                hauteur = 0;
            }
        } else {
            vitesse += accelerationPoney();
        }
        
        progression += vitesse / 120;
        
        notifyObservers(this);
    }
    
    /**
     * Donne la possible prochaine position d'un poney.
     * @return prochain position du poney this
     */
    double getNextProg() {
        double accelerationNext = (vitesseMax - vitesse) / 1200;
        double vitesseNext = vitesse + accelerationNext;
        return progression + vitesseNext / 120;
    }
    
    void incrementerTour() {
        nbTours++;
        progression = 0;
    }
    
    /**
     * Effet de la barrière sur le poney.
     * @param b barriere concernée
     * @return retourne l'état de la barrière après passage
     */
    public boolean barriere(Barriere b) {
        if (hauteur < b.getHauteur()) {
            vitesse *= 0.6;
        }
        return false;
    }
    
    /**
     * Effet de la rivière sur le poney.
     */
    public void riviere() {
        vitesse *= 0.7;
    }
    
    /**
     * Effet du ravin sur le poney.
     */
    public void ravin() {
        if (hauteur == 0) {
            progression += 0.1;
            vitesse = 0;
            hauteur = 0;
        }
    }
    
    /**
     * Met à jour le classement du poney.
     * @param i nouveau classement
     */
    void updatePos(int i) {
        if (i != classement) {
            this.classement = i;
            notifyObservers(this);
        }
    }


    public double getVitesse() {
        return vitesse;
    }

    public void setVitesse(double vitesse) {
        this.vitesse = vitesse;
    }

    public int getClassement() {
        return classement;
    }


    /**
     * Mets le poney en état de saut si il est au sol.
     */
    public void jump() {
        if (hauteur == 0) {
            isJumping = true;
        }
    }

    public double getHauteur() {
        return hauteur;
    }

    public String getNormalAssetPath() {
        return normalAssetPath;
    }

    public String getRainbowAssetPath() {
        return rainbowAssetPath;
    }

    /**
     * Remet tout à 0 en début de partie.
     */
    public void reset() {
        progression = 0;
        acceleration = 0;
        vitesse = 0;
        nbTours = 0;
    }

    public String getType() {
        return type;
    }

    public int getNbTours() {
        return nbTours;
    }

    public void setType(String type) {
        this.type = type;
    }

    void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }
    
    
}
