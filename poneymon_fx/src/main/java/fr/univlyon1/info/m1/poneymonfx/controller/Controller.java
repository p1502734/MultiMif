package fr.univlyon1.info.m1.poneymonfx.controller;

import fr.univlyon1.info.m1.poneymonfx.model.Game;
import fr.univlyon1.info.m1.poneymonfx.network.ClientMessage;
import fr.univlyon1.info.m1.poneymonfx.network.GameClient;
import fr.univlyon1.info.m1.poneymonfx.view.View;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.animation.AnimationTimer;

/**
 * Controller.
 *
 * @author Jordan
 */
public class Controller {

    private final List<View> vues;
    private final AnimationTimer timer;
    private GameClient client;

    /**
     * Constructeur.
     */
    public Controller() {
        vues = new ArrayList<>();
        timer = new AnimationTimer() {
            @Override
            public void handle(long currentNanoTime) {
                vues.forEach(View::display);
            }
        };
    }

    /**
     * Ajoute le game au controller.
     * @param game le game du serveur.
     */
    public void setGame(Game game) {
        this.client = game.getClient();
    }

    /**
     * Lancer le jeu.
     */
    public void startTimer() {
        timer.start();
    }


    /**
     * Envoie le message pour rejouer en fin de partie.
     */
    public void rejouer() {
        try {
            client.sendSignal(ClientMessage.REPLAY);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addView(View vue) {
        vues.add(vue);
    }

    /**
     * Envoie le message pour sauter.
     */
    public void jump() {
        try {
            client.sendSignal(ClientMessage.JUMP);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envoie le message pour utiliser son pouvoir.
     */
    public void power() {
        try {
            client.sendSignal(ClientMessage.POWER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envoie un message pour lancer la partie.
     */
    public void start() {
        try {
            client.sendSignal(ClientMessage.START_GAME);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Envoie un message pour changer de role joueur/spectateur.
     */
    public void switchRole() {
        try {
            client.sendSignal(ClientMessage.SWITCH);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deconnecte le client, et envoie un message au serveur s'il est connecté.
     */
    public void disconnect() {
        client.clean();
        try {
            client.quit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
