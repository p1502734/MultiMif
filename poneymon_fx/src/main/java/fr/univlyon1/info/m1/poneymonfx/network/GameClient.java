package fr.univlyon1.info.m1.poneymonfx.network;

import fr.univlyon1.info.m1.poneymonfx.model.Field;
import fr.univlyon1.info.m1.poneymonfx.model.Game;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.FieldMsg;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.IdMsg;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.RaceEndMsg;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.ServerInfoMsg;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.ServerMessage;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.UpdateBarrieresMsg;
import fr.univlyon1.info.m1.poneymonfx.network.servermessages.UpdatePoneysMsg;
import fr.univlyon1.info.m1.poneymonfx.tools.Observable;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.NetworkInterface;
import java.net.InterfaceAddress;
import java.net.SocketTimeoutException;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

/**
 * Classe représentant un client du jeu. Permet de rechercher, se connecter à un serveur.
 * Met à jour le modèle du jeu.
 */
public class GameClient extends Observable {

    private Socket s;
    private DataOutputStream dos;

    private boolean connected = false;

    private Field field = null;
    private int id = -2;
    private ReceiveServerObjectsThread receiveObjectsThread;

    private final Game game;

    public GameClient(Game game) {
        this.game = game;
    }

    /**
     * Connexion à un serveur.
     *
     * @param host L'adresse du serveur.
     * @param port Le port du serveur.
     * @return true si on est connecté, false sinon
     * @throws IOException En cas d'erreur sur la connexion.
     */
    private boolean connect(String host, int port) throws IOException {
        s = new Socket(host, port);
        ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
        dos = new DataOutputStream(s.getOutputStream());
        if (s.isClosed()) {
            return false;
        } else {
            //On doit envoyer notre userModel au serveur, précédé d'un signal
            sendSignal(ClientMessage.USER_MODEL_READY_TO_RECEIVE);

            ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
            oos.writeUnshared(game.getUser());

            //La socket pourrait être fermée
            if (s.isClosed()) {
                return false;
            }

            connected = true;

            //On instancie notre thread qui écoute le serveur, et se souvient des messages
            //Renommage de la classe ?
            receiveObjectsThread = new ReceiveServerObjectsThread(ois, this);

            Thread thread = new Thread(receiveObjectsThread);
            thread.start();

            //We wait for the id
            //We should normally get the id by receiveObjectsThread
            while (id != -2) {
            }

        }
        return true;
    }

    /**
     * Rejoint un serveur, à partir d'un objet Server.
     *
     * @param server Le serveur.
     */
    public void joinServer(ServerInfo server) {
        joinServer(server.address, server.port);
    }

    /**
     * Se connecte au serveur et entre dans le lobby.
     *
     * @param host L'adresse du serveur
     * @param port Le port du serveur
     */
    public void joinServer(String host, int port) {
        try {
            if (!connect(host, port)) {
                quit();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    // -------- processMessage

    /**
     * Appelle la fonction correspondante au type du serverMessage.
     * @param msg le message à traiter.
     */
    void processMessage(ServerMessage msg) {
        if (msg instanceof IdMsg) {
            processMessage((IdMsg) msg);
        } else if (msg instanceof FieldMsg) {
            processMessage((FieldMsg) msg);
        } else if (msg instanceof UpdatePoneysMsg) {
            processMessage((UpdatePoneysMsg) msg);
        } else if (msg instanceof ServerInfoMsg) {
            processMessage((ServerInfoMsg) msg);
        } else if (msg instanceof UpdateBarrieresMsg) {
            processMessage((UpdateBarrieresMsg) msg);
        } else if (msg instanceof RaceEndMsg) {
            processMessage();
        }
    }

    private void processMessage(FieldMsg msg) {
        field = new Field(msg.field);
        field.setPoneyFocused(id);
        //Notification à la vue qu'on a créé le field
        notifyObservers(field);

        //On créé le field et on envoie un message au serveur lorsque le jeu est prêt
        //Attente d'un message de la vue ?
        try {
            sendSignal(ClientMessage.READY);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processMessage(IdMsg msg) {
        setNewId(msg.id);
    }

    private void processMessage() {
        field.finCourse();
    }

    private void processMessage(UpdatePoneysMsg msg) {
        field.update(msg.pMap);
    }
    
    private void processMessage(UpdateBarrieresMsg msg) {
        field.updateObstacles(msg.oMap);
    }

    private void processMessage(ServerInfoMsg msg) {
        notifyObservers(msg.info);
    }

    // -------- End processMessage

    /**
     * Attribue un nouvel identifiant obtenu du serveur.
     *
     * @param id Le nouvel id
     */
    private void setNewId(int id) {
        //On doit normalement recevoir notre identifiant par le serveur.
        // 0 : Administrateur
        // -1 : Spectateur
        // Autre : Joueur
        this.id = id;
        notifyObservers(id);
    }

    public int getId() {
        return id;
    }

    /**
     * Envoie un signal au serveur.
     *
     * @param s Le message à envoyer
     * @throws IOException Si problème avec le dataOutputStream
     */
    public void sendSignal(ClientMessage s) throws IOException {
        dos.write(s.getValue());
        dos.flush();
    }

    /**
     * Envoie le signal de déconnection au serveur et ferme la socket.
     *
     * @throws IOException if an I/O error occurs when closing this socket.
     */
    public void quit() throws IOException {
        if (connected) {
            sendSignal(ClientMessage.DISCONNECT);
            s.close();
            receiveObjectsThread.stop();
            connected = false;
        }
    }

    /**
     * Recherche de serveurs en broadcast.
     * N'utilise pas ReceiveServerObjectsThread car passe par une socket UDP.
     *
     * @return la liste des serveurs.
     */
    public List<ServerInfo> findServers() {
        List<ServerInfo> res = new LinkedList<>();
        // Find the server using UDP broadcast
        try {
            //Open a random port to send the package
            DatagramSocket c = new DatagramSocket();
            c.setBroadcast(true);
            c.setSoTimeout(3000);
            byte[] sendData = "DISCOVER_SERVER_REQUEST".getBytes();

            //Try the 255.255.255.255 first
            try {
                DatagramPacket sendPacket = new DatagramPacket(sendData,
                        sendData.length,
                        InetAddress.getByName("255.255.255.255"), 8888);
                c.send(sendPacket);
            } catch (Exception e) {
                System.out.println(Arrays.toString(e.getStackTrace()));
            }

            // Broadcast the message over all the network interfaces
            Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface networkInterface = (NetworkInterface) interfaces.nextElement();

                if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                    continue; // Don't want to broadcast to the loopback interface
                }

                for (InterfaceAddress interfaceAddress :
                        networkInterface.getInterfaceAddresses()) {
                    InetAddress broadcast = interfaceAddress.getBroadcast();
                    if (broadcast == null) {
                        continue;
                    }

                    // Send the broadcast package!
                    try {
                        DatagramPacket sendPacket = new DatagramPacket(sendData,
                                sendData.length,
                                broadcast,
                                8888);
                        c.send(sendPacket);
                    } catch (Exception e) {
                        System.out.println(Arrays.toString(e.getStackTrace()));
                    }

                }
            }

            //Wait for a response
            byte[] recvBuf = new byte[15000];
            DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
            try {
                c.receive(receivePacket);
            } catch (SocketTimeoutException e) {
                return null;
            }
            //We have a response

            //Check if the message is correct
            String[] message = new String(receivePacket.getData()).trim().split(";");
            if (message[0].equals("DISCOVER_SERVER_RESPONSE")) {
                // Le message est correct, on essaie de parser le message pour créer le serveur
                ServerInfo si = new ServerInfo(message[1]);
                res.add(si); //On l'ajoute à la liste
            }
            //Close the port!
            c.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return res;
    }

    // Accesseurs

    public boolean isConnected() {
        return connected;
    }

    public Field getField() {
        return field;
    }

    public void clean() {
        field = null;
    }
}
