package fr.univlyon1.info.m1.poneymonfx.view.gameplay;

import fr.univlyon1.info.m1.poneymonfx.model.Game;
import fr.univlyon1.info.m1.poneymonfx.model.PoneyFactory;
import fr.univlyon1.info.m1.poneymonfx.network.ServerInfo;
import fr.univlyon1.info.m1.poneymonfx.view.View;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class MenuView extends BorderPane implements View {

    private final int width;
    private final int height;

    private JfxView jfxView;
    private final Game game;

    private final GridPane grid = new GridPane();

    private final Image image1 = new Image("assets/pony-purple-running.gif");
    private final Image image2 = new Image("assets/pony-pegase-running.gif");
    private final Image image3 = new Image("assets/pony-ghost-running.gif");
    private final Image image4 = new Image("assets/pony-tank-running.gif");
    private final Image image5 = new Image("assets/pony-aqua-running.gif");

    private final Image[] listOfImage = {image1, image2, image3, image4, image5};

    /**
     * Controleur.
     *
     * @param w largeur
     * @param h hauteur
     * @param game le jeu pour pouvoir modifier userModel (à changer via controleur?)
     */
    MenuView(int w, int h, Game game) {
        this.game = game;
        width = w;
        height = h;

        this.setPrefHeight(height);
        this.setPrefWidth(width);

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
    }

    @Override
    public void display() {
        displayMenu();
    }

    /**
     * Affichage du menu principal.
     */
    void displayMenu() {
        clearBorderPanel();
        Image fond = new Image("assets/FondMenu.png", width, height, false, true);
        BackgroundSize bSize = new BackgroundSize(
                BackgroundSize.AUTO,
                BackgroundSize.AUTO, true, true, true, false);

        this.setBackground(new Background(new BackgroundImage(fond,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                bSize)));

        Button button1 = new Button("Jouer");
        Button button2 = new Button("Quitter");
        button1.setPrefSize(width * 0.3, height * 0.2);
        button2.setPrefSize(width * 0.3, height * 0.2);

        VBox vbButtons = new VBox();
        vbButtons.setAlignment(Pos.CENTER_RIGHT);
        vbButtons.setSpacing(10);
        vbButtons.setPadding(new Insets(0, 20, 10, 20));
        vbButtons.getChildren().addAll(button1, button2);
        Image img1 = new Image("assets/pony-aqua-running.gif",
                width * 0.6, height * 0.6, false, true);
        ImageView imageView = new ImageView(img1);
        this.setLeft(imageView);
        this.setCenter(vbButtons);

        button1.setOnAction((ActionEvent e)
                -> displayJoueurMenu()
        );

        button2.setOnAction((ActionEvent e) -> {
            Platform.exit();
            System.exit(0);
        });
    }

    /**
     * Affichage profil Joueur.
     */
    private void displayJoueurMenu() {
        clearBorderPanel();
        Text scenetitle = new Text("Bonjour !");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        grid.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("Pseudo :");
        grid.add(userName, 0, 1);

        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 1);

        //Liste des Poneys
        ListView<String> listView = new ListView<>();
        ObservableList<String> items = FXCollections.observableArrayList(
                "NIAN", "PEGASE", "GHOST", "TANK", "AQUA");

        listView.setItems(items);

        listView.setCellFactory(param -> new ListCell<String>() {
            private final ImageView imageView = new ImageView();

            @Override
            public void updateItem(String name, boolean empty) {
                super.updateItem(name, empty);

                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else {
                    //TODO changer via enum?
                    switch (name.toUpperCase()) {
                        case "NIAN":
                            imageView.setImage(listOfImage[0]);
                            break;
                        case "PEGASE":
                            imageView.setImage(listOfImage[1]);
                            break;
                        case "GHOST":
                            imageView.setImage(listOfImage[2]);
                            break;
                        case "TANK":
                            imageView.setImage(listOfImage[3]);
                            break;
                        case "AQUA":
                            imageView.setImage(listOfImage[4]);
                            break;
                        default:
                            imageView.setImage(null);
                    }
                    setText(name);
                    setGraphic(imageView);
                }
            }
        });

        this.setCenter(listView);

        Button btn = new Button("Confirmer");
        Button btn2 = new Button("Retour");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().addAll(btn2, btn);
        grid.add(hbBtn, 1, 4);

        btn.setOnAction((ActionEvent e) -> {
            //Creation du joueur
            if ((!userTextField.getText().equals(""))
                    && (listView.getSelectionModel().getSelectedIndex() != -1)) {
                try {
                    game.setUserModel(userTextField.getText(),
                            listView.getSelectionModel().getSelectedIndex());
                } catch (PoneyFactory.PoneyCreationException e1) {
                    e1.printStackTrace();
                }
                System.out.println(game.getUser().getCouleurPoney());

                displayChoixTypeJoueur();
            }
        });

        btn2.setOnAction((ActionEvent i) -> displayMenu());

        this.setBackground(
                new Background(
                        new BackgroundFill(
                                Color.LIGHTGRAY,
                                new CornerRadii(2),
                                new Insets(2))));
        this.setRight(grid);
    }

    /**
     * Vue du choix serveur ou client.
     */
    void displayChoixTypeJoueur() {
        clearBorderPanel();
        Text scenetitle = new Text("Bonjour " + game.getUser().getPseudo() + " !");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(scenetitle, 0, 0, 2, 1);

        Button btn = new Button("Créer une partie");
        Button btn2 = new Button("Rejoindre une partie");
        Button btn3 = new Button("Retour");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().addAll(btn, btn2, btn3);
        grid.add(hbBtn, 1, 4);

        setCenter(grid);

        btn.setOnAction((ActionEvent i) -> displayCreationPartie());

        btn2.setOnAction((ActionEvent i) -> displayRejoindPartie());

        btn3.setOnAction((ActionEvent i) -> {
            game.getUser().remove();
            displayJoueurMenu();
        });

    }

    /**
     * Vue creation de partie (serveur).
     */
    private void displayCreationPartie() {
        clearBorderPanel();
        Text scenetitle = new Text("Création d'une partie !");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        grid.add(scenetitle, 0, 0, 2, 1);

        Label labelNomPartie = new Label("Nom de la partie :");
        grid.add(labelNomPartie, 0, 1);

        TextField textFieldNomPartie = new TextField();
        grid.add(textFieldNomPartie, 1, 1);

        Button btn = new Button("Confirmer");
        Button btn2 = new Button("Retour");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().addAll(btn2, btn);
        grid.add(hbBtn, 1, 4);

        btn.setOnAction((ActionEvent i) -> {
            //Creation du joueur
            if ((!textFieldNomPartie.getText().equals(""))) {

                //TODO AFFICHAGE LOBBY
                //Pour l'instant serveur en console... On ferme la fenetre
                //TODO Choix nbPlayers + spec + port?
                game.createServerAndJoin(textFieldNomPartie.getText(), 4500, 5, 5);
                jfxView.showLobbyView();

            }
        });

        btn2.setOnAction((ActionEvent i) -> displayChoixTypeJoueur());

        this.setBackground(
                new Background(
                        new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(2), new Insets(2))));
        setCenter(grid);
    }

    /**
     * Vue rejoindre une partie (client).
     */
    private void displayRejoindPartie() {
        clearBorderPanel();
        Text scenetitle = new Text("Rejoindre une partie !");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(scenetitle, 0, 0, 2, 1);

        Button btn = new Button("Rejoindre par liste");
        Button btn2 = new Button("Rejoindre par IP");
        Button btn3 = new Button("Retour");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().addAll(btn, btn2, btn3);
        grid.add(hbBtn, 1, 4);

        setCenter(grid);

        btn.setOnAction((ActionEvent i) -> displayRejoindPartieList());

        btn2.setOnAction((ActionEvent i) -> displayRejoindPartieIp());

        btn3.setOnAction((ActionEvent i) -> displayChoixTypeJoueur());
    }

    /**
     * Rejoindre une partie en entrant une adresse IP.
     */
    private void displayRejoindPartieIp() {
        clearBorderPanel();
        Text scenetitle = new Text("Rejoindre une partie par IP!");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        grid.add(scenetitle, 0, 0, 2, 1);

        Label labelNomPartie = new Label("Ip du serveur :");
        grid.add(labelNomPartie, 0, 1);
        TextField textFieldNomPartie = new TextField();
        grid.add(textFieldNomPartie, 1, 1);

        Label labelPort = new Label("Port :");
        grid.add(labelPort, 0, 2);
        TextField textFieldPort = new TextField();
        // force the field to be numeric only
        textFieldPort.textProperty()
                .addListener((ObservableValue<? extends String> observable,
                        String oldValue, String newValue) -> {
                    if (!newValue.matches("\\d*")) {
                        textFieldPort.setText(newValue.replaceAll("[^\\d]", ""));
                    }
                });
        grid.add(textFieldPort, 1, 2);

        Button btn = new Button("Confirmer");
        Button btn2 = new Button("Retour");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().addAll(btn2, btn);
        grid.add(hbBtn, 1, 4);

        btn.setOnAction((ActionEvent i) -> {
            if ((!textFieldNomPartie.getText().equals(""))
                    && (!textFieldPort.getText().equals(""))) {

                game.joinServer(textFieldNomPartie.getText(),
                        Integer.parseInt(textFieldPort.getText()));
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
                if (game.getClient().isConnected()) {
                    jfxView.showLobbyView();
                } else {
                    Stage dialog = new Stage();
                    dialog.initModality(Modality.APPLICATION_MODAL);
                    VBox dialogVbox = new VBox(20);
                    Button bClose = new Button("Fermer");
                    dialogVbox.getChildren().addAll(new Text("Serveur introuvable !"), bClose);
                    dialogVbox.setAlignment(Pos.CENTER);
                    Scene dialogScene = new Scene(dialogVbox, 300, 200);
                    dialog.setScene(dialogScene);
                    dialog.show();

                    bClose.setOnAction((ActionEvent u) -> dialog.close());
                }
            }
        });

        btn2.setOnAction((ActionEvent i) -> displayRejoindPartie());

        this.setBackground(
                new Background(
                        new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(2), new Insets(2))));
        setCenter(grid);
    }

    /**
     * Rejoindre une partie à partir d'une liste.
     */
    private void displayRejoindPartieList() {
        clearBorderPanel();
        Text scenetitle = new Text("Liste des serveurs !");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        grid.add(scenetitle, 0, 0, 2, 1);

        //Liste des Poneys
        ListView<String> listView = new ListView<>();
        ObservableList<String> items = FXCollections.observableArrayList();

        List<ServerInfo> serverInfoList = game.findServers();

        if (serverInfoList == null) {
            items.add("Pas de serveur trouvé...");
        } else {
            for (ServerInfo aServerInfoList : serverInfoList) {
                items.add(aServerInfoList.getName()
                        + " ----- Joueur(s) : " + aServerInfoList.getNbPlayers()
                        + "/" + aServerInfoList.getNbPlayersMax()
                        + " ----- Spectateur(s) : " + aServerInfoList.getNbSpectators()
                        + "/" + aServerInfoList.getNbSpectatorsMax()
                );
            }
        }

        listView.setItems(items);

        this.setCenter(listView);

        Button btn = new Button("Rejoindre");
        Button btn3 = new Button("Recherche serveur");
        Button btn2 = new Button("Retour");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().addAll(btn2, btn3, btn);
        grid.add(hbBtn, 1, 4);

        btn.setOnAction((ActionEvent e) -> {
            //Creation du joueur
            if (serverInfoList != null && (listView.getSelectionModel().getSelectedIndex() != -1)) {
                game.joinServer(serverInfoList.get(
                        listView.getSelectionModel().getSelectedIndex()
                ));
                jfxView.showLobbyView();
            }
        });

        btn3.setOnAction((ActionEvent i) -> displayRejoindPartieList());

        btn2.setOnAction((ActionEvent i) -> displayRejoindPartie());

        this.setBackground(
                new Background(
                        new BackgroundFill(
                                Color.LIGHTGRAY,
                                new CornerRadii(2),
                                new Insets(2))));
        this.setRight(grid);
    }

    /**
     * Vider le borderPanel pour afficher d'autres objets.
     */
    private void clearBorderPanel() {
        this.setLeft(null);
        this.setRight(null);
        this.setCenter(null);
        grid.getChildren().clear();

    }

    /**
     * Recupere le jfxView.
     *
     * @param jfxView vue principale
     */
    void setJfxView(JfxView jfxView) {
        this.jfxView = jfxView;
    }
}
