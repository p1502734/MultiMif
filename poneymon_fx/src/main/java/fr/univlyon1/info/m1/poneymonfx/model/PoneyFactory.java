package fr.univlyon1.info.m1.poneymonfx.model;

import fr.univlyon1.info.m1.poneymonfx.model.poney.AquaPoney;
import fr.univlyon1.info.m1.poneymonfx.model.poney.GhostPoney;
import fr.univlyon1.info.m1.poneymonfx.model.poney.NianPoney;
import fr.univlyon1.info.m1.poneymonfx.model.poney.PegasePoney;
import fr.univlyon1.info.m1.poneymonfx.model.poney.TankPoney;

/**
 * PoneyFactory.
 * @author Jordan
 */
public class PoneyFactory {
    
    private PoneyFactory() {
        
    }

    /**
     * Instancie un poney.
     * @param poneyType type de poney à créer
     * @return instance de poney
     * @throws PoneyFactory.PoneyCreationException type de poney non valide
     */
    static Poney create(String poneyType)
            throws PoneyCreationException {
        switch (poneyType) {
            case "Aqua":
                return new AquaPoney();
            case "Ghost":
                return new GhostPoney();
            case "Nian":
                return new NianPoney();
            case "Pegase":
                return new PegasePoney();
            case "Tank":
                return new TankPoney();
            default:
                throw new PoneyCreationException(
                        "Impossible de créer un poney " + poneyType);
        }
    }

    public static class PoneyCreationException extends Exception {

        PoneyCreationException(String errorMessage) {
            super(errorMessage);
        }
    }

}
