package fr.univlyon1.info.m1.poneymonfx.model;

import fr.univlyon1.info.m1.poneymonfx.tools.Observable;

import java.io.Serializable;

/**
 * GameObject.
 * @author Jordan
 */
public abstract class GameObject extends Observable implements Serializable {

    protected double progression;
    private final int id;
    static int nbObj = 0;

    GameObject() {
        this.id = ++nbObj;
    }

    public double distanceTo(GameObject obj) {
        return obj.progression - this.progression;
    }

    public int getId() {
        return id;
    }

    public double getProgression() {
        return progression;
    }
    
    

}
